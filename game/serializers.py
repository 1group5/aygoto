from rest_framework import serializers

from .models import Level


class LevelSerializer(serializers.ModelSerializer):
    """
    Serialize Level Objects
    """

    class Meta:
        model = Level
        fields = "__all__"
