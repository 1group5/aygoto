///
///
///
///                                                                                   tttt
///                                                                                ttt:::t
///                                                                                t:::::t
///                                                                                t:::::t
///  aaaaaaaaaaaaayyyyyyy           yyyyyyyggggggggg   ggggg   ooooooooooo   ttttttt:::::ttttttt       ooooooooooo
///  a::::::::::::ay:::::y         y:::::yg:::::::::ggg::::g oo:::::::::::oo t:::::::::::::::::t     oo:::::::::::oo
///  aaaaaaaaa:::::ay:::::y       y:::::yg:::::::::::::::::go:::::::::::::::ot:::::::::::::::::t    o:::::::::::::::o
///           a::::a y:::::y     y:::::yg::::::ggggg::::::ggo:::::ooooo:::::otttttt:::::::tttttt    o:::::ooooo:::::o
///    aaaaaaa:::::a  y:::::y   y:::::y g:::::g     g:::::g o::::o     o::::o      t:::::t          o::::o     o::::o
///  aa::::::::::::a   y:::::y y:::::y  g:::::g     g:::::g o::::o     o::::o      t:::::t          o::::o     o::::o
/// a::::aaaa::::::a    y:::::y:::::y   g:::::g     g:::::g o::::o     o::::o      t:::::t          o::::o     o::::o
///a::::a    a:::::a     y:::::::::y    g::::::g    g:::::g o::::o     o::::o      t:::::t    tttttto::::o     o::::o
///a::::a    a:::::a      y:::::::y     g:::::::ggggg:::::g o:::::ooooo:::::o      t::::::tttt:::::to:::::ooooo:::::o
///a:::::aaaa::::::a       y:::::y       g::::::::::::::::g o:::::::::::::::o      tt::::::::::::::to:::::::::::::::o
/// a::::::::::aa:::a     y:::::y         gg::::::::::::::g  oo:::::::::::oo         tt:::::::::::tt oo:::::::::::oo
///  aaaaaaaaaa  aaaa    y:::::y            gggggggg::::::g    ooooooooooo             ttttttttttt     ooooooooooo
///                     y:::::y                     g:::::g
///                    y:::::y          gggggg      g:::::g
///                   y:::::y           g:::::gg   gg:::::g
///                  y:::::y             g::::::ggg:::::::g
///                 yyyyyyy               gg:::::::::::::g
///                                         ggg::::::ggg
///                                            gggggg
///
///
/// >>> The aygoto game <<<
///
///
var height = window.innerHeight;
var width = window.innerWidth;
var newBelow = below;                // unused
var newEquals = equals;              // unused


// const url = new URL("http://127.0.0.1:8000/api/level/");

//var empty = lookup.below[0];
//var water = lookup.below[1];
//var road = lookup.below[2];
//var railway = lookup.below[3];
//console.log("[DBG]: empty: " + empty);

var config = {
  type: Phaser.AUTO,
  physics: {
    default: 'arcade',
    arcade: {
      gravity: { y: 0 }
    },
  },
  scale: {
    mode: Phaser.Scale.RESIZE,
    autoCenter: Phaser.Scale.CENTER_BOTH,
    width: '100%',
    height: '100%'
  },
  scene: {
    preload: preload,
    create: create,
    update: update
  }
};
let controls;


/*
 DEPRECATED

// load via fetch-API
console.log(url);
fetch(url)
  .then(response => response.json())
  //  .then(edata => console.log(edata))
  .then(data => {
    console.log("setting up new below");
    newBelow = data[14].data.below.map(row => row.map(value => {

      switch (value) {
        case 0:
          return 47;
        case 1:
          return 486;
        case 2:
          return 73;
        case 3:
          return 54;
        default:
          throw 'value not able to be mapped';
      }
    }));
    console.log(newBelow);
    var game = new Phaser.Game(config);
    return data;
  })
  .then(data => {
    console.log("setting up new equals");
    newEquals = data[6].data.equals.map(row => row.map(value => {
      switch (value) {
        case 1:
          return 182;
      }
    }));
    console.log(newEquals);
  })
  .catch(error => console.log(error))
  .finally(() => console.log("Oh my, finally"));
*/

// NOTE: for this to work, you need to declare the assets from the template as var,
// because they need to get the correct path then being used.

///////////////////////////////////////////////////////////////////////////////////////////
// GAME VARS
///////////////////////////////////////////////////////////////////////////////////////////
var game = new Phaser.Game(config);
var map;


/**
 * Phaser3 Game Lifecycle Hook
 *
 * This happens once before the intialization phase.
 * Phaser gets told which static assets it should load and keep track of.
 * */
function preload() {
  this.load.image('silver-tiles', tileset);
  this.load.spritesheet('charakter', charakter, { frameWidth: 32, frameHeight: 32 });
  this.load.audio('music', music);
    //Music: https://www.chosic.com/free-music/all/ 
}

/**
 * Phaser3 Game Lifecycle Hook
 *
 * This happens only once after 'preload()' is done.
 * This function is responsible for initializing the game, constructing the map from level data,
 * configuring physics, and adding the player to the world.
 */
function create() {

  var music;
  music = this.sound.add('music');
  music.play();
  // DEPRECATED
  // old:
  //     const map = this.make.tilemap({key: 'world'});
  //     const tileset = map.addTilesetImage("32x32_tilemap", 'silver-tiles');

  // merging arrays as quick fix
  //for (let i = 0; i < newBelow.length; i++) {
    //for (let j = 0; j < newBelow.length; j++) {
      //if (newEquals[i][j] == 182) {
        //newBelow[i][j] = 182;
      //}
    //}
  //}

  // LEGACY IMPLEMENTATION
  //var map = this.make.tilemap({ data: newBelow, tileWidth: 32, tileHeight: 32 });
  //var tiles = map.addTilesetImage('silver-tiles');
  //var layer = map.createLayer(0, tiles, 0, 0);

  // NEW IMPLEMENTATION
  //map = this.make.tilemap({ data: below, tileWidth: 32, tileHeight: 32 , width: 100, height: 100 });
  map = this.make.tilemap({ tileWidth: 32, tileHeight: 32 , width: 100, height: 100 });
  var tileset = map.addTilesetImage('silver-tiles');

  //var belowLayer = map.createBlankLayer('below', tileset, 0, 0);
  //var equalsLayer = map.createBlankLayer('equals', tileset, 0, 0);
  //var aboveLayer = map.createBlankLayer('above', tileset, 0, 0);

  // ORIG:
  var belowLayer = map.createBlankLayer('below', tileset, 0, 0);
  var equalsLayer = map.createBlankLayer('equals', tileset, 0, 0);
  //var aboveLayer = map.createBlankLayer('above', tileset, 0, 0);

  for (var i = 0; i < below.length; i++) {
    for (var j = 0; j < below[0].length; j++) {
      let index = lookup_mapping(below[i][j], 'below');
      belowLayer.putTileAt(index, i, j);
      //dbgBelow[i][j];
    }
  }
  console.log("[DBG] below: " + below);

  // FIXME: commenting in the code for the equals layer alters the map
  // TODO: clarify how this impacts map display?
  //
  // do buldings overshadow streets?
  // do they somehow interefere with the tilemaps' indides?
  //
  //
  // first suspect: encoding of 'equals' layer
  for (var i = 0; i < equals.length; i++) {
    for (var j = 0; j < equals[0].length; j++) {
      let index = lookup_mapping(equals[i][j], 'equals');
      if (index == 90) {
        equalsLayer.putTileAt(index, i, j);
      }
    }
  }
  //console.log("[DBG] equals: " + equals);


  map.setLayer(equalsLayer);

  // DEPRECATED
  // as it should:
  //var below = map.createLayer('below', tiles, 0, 0);
  //var equals = map.createLayer('equals', tiles, 0, 0);
  //var above = map.createLayer('above', tiles, 0, 0);

  //     const belowLayer = map.createLayer("Below Player", tileset, 0, 0);
  //     // HERE....

  //     // TODO: also do this for the other layers once data for them is ready, too...
  //     const worldLayer = map.createLayer("World", tileset, 0, 0);
  //     const aboveLayer = map.createLayer('Above Player', tileset, 0, 0);

  //     aboveLayer.setDepth(10);
  //     const dekoLayer = map.createLayer('Deko', tileset, 0, 0);
  //     dekoLayer.setDepth(11);


  this.physics.world.setBounds(0, 0, 3200, 3200);
  //camera shenanigans
  const camera = this.cameras.main;
  cursors = this.input.keyboard.createCursorKeys();
  camera.setBounds(0, 0, 3200, 3200);

  // functions for animation
  this.anims.create({
    key: "left",
    frameRate: 4,
    frames: this.anims.generateFrameNumbers("charakter", { start: 3, end: 5 }),
    repeat: -1
  });

  this.anims.create({
    key: "right",
    frameRate: 4,
    frames: this.anims.generateFrameNumbers("charakter", { start: 6, end: 8 }),
    repeat: -1
  });

  this.anims.create({
    key: "up",
    frameRate: 4,
    frames: this.anims.generateFrameNumbers("charakter", { start: 9, end: 11 }),
    repeat: -1
  });

  this.anims.create({
    key: "down",
    frameRate: 4,
    frames: this.anims.generateFrameNumbers("charakter", { start: 0, end: 2 }),
    repeat: -1
  });

  this.anims.create({
    key: "stand",
    frameRate: 4,
    frames: this.anims.generateFrameNumbers("charakter", { start: 1, end: 1 }),
    repeat: -1
  });
  this.player = this.physics.add.sprite(0, 0, 'charakter');
  this.player.setCollideWorldBounds(true);
  camera.startFollow(this.player, true);

  // DEPRECATED
  // collision things - the first sets colliding tiles by indices (should be all in the layer, which are the houses), second should enable the player colliding with them
  //when collider gets enabled, player wont move anymore
  //map.setCollisionBetween(1, 999, true, 'worldLayer');
  // => bc every tile is set as colliding and the player gets trapped between colliding tiles? ;)
  //
  ////////////////////////////////////////////////////////////
  // Try:
  //AUS
  //worldLayer.setCollisionByProperty({ collides: true });
  //dekoLayer.setCollisionByProperty({ collides: true });
  //NOTE:
  //this would require Julia to add a custom 'collides: true' property on all the House and Tree tiles inside the the World Layer.
  ////////////////////////////////////////////////////////////

  //this.physics.add.collider(this.player, dekoLayer);

  //Should show colliding tiles like so:
  // const debugGraphics = this.add.graphics().setAlpha(0.75).setDepth(20);
  // worldLayer.renderDebug(debugGraphics, {
  //   tileColor: null,
  //   collidingTileColor: new Phaser.Display.Color(243, 134, 48, 255),
  //   faceColor: new Phaser.Display.Color(40, 39, 37, 255)
  // });
  //var collidings = [];
//
  //for (let i = 0; i < newBelow.length; i++) {
    //for (let j = 0; j < newBelow.length; j++) {
      //if (newBelow[i][j] == 182) {
        //collidings.push(map.getTileAt(i, j));
      //}
    //}
  //}
//
  //this.physics.collideTiles(this.player, collidings);
//

  ///////////////////////////////////////////////////////////////////////////////////
  // vars for movment
  ///////////////////////////////////////////////////////////////////////////////////
  var infovis = true;
  leftKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT);
  rightKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
  upKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP);
  downKey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN);
  var spacekey = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);

  lines = [
    'Standort: Hochschule Bochum',
    'Maße: 1 Kachel entspricht 1m²',
    'Infofenster mit Leerzeichen ein- und ausblenden.'
  ];

  text = this.add.text(16, 16, lines, {
    fontSize: '20px',
    padding: { x: 20, y: 10 },
    backgroundColor: '#000000',
    fill: '#ffffff'
  });


  spacekey.on('down', function (key, event) {
    if (infovis) {
      text.visible = false;
      infovis = false;
    }
    else {
      text.visible = true;
      infovis=true;
    }
  });


}

/**
 * Phaser3 Game Lifecycle Hook
 *
 * This functions happens on every frame/ game-tick.
 *
 * It is mainly responsible for handling user input and adjusting the players speed in the world.
 * */

//after fixing animation stop bug. i found out that changing the movement rules makes the code
//simpler and fixes the bug. 
//if new rules not wanted:
//create button matrix array and if one clicked the animations "stand" is blocked.
function update() {
  this.player.setVelocity(0);
  if (cursors.up.isDown) {
    this.player.setVelocityY(-100000);
    this.player.anims.play("up", true);
  }
  else if (cursors.down.isDown) {
    this.player.setVelocityY(100000);
    this.player.anims.play("down", true);
  }
  else if (cursors.left.isDown) {
    this.player.setVelocityX(-100000);
    this.player.anims.play("left", true);
  }
  else if (cursors.right.isDown) {
    this.player.setVelocityX(100000);
    this.player.anims.play("right", true);
  } else{this.player.anims.play("stand", true);
  }
}


////////////////////////////////////////////////////////////////
// UTILS
////////////////////////////////////////////////////////////////

/**
 * Util -- interface with lookup dir
 *
 * this function matches the static Map Data Content encoding against the local lookup.json in order to make the Tilemap in use interchangable
 *
 * TODO: have a look whether docstring is alright
 *
 * @param code  -- the data code we want to look up
 * @param layer -- the layer to search in. Can be 'below', 'equals' or 'above'
 * @return s    -- the level data encoding, or -1 if lookup fails
 * */
function lookup_mapping(code, layer) {
  //console.log("[DBG] lookup code: " + code);

  var s;

  if (layer === 'below') {
    s = lookup.below[String(code)];
  }
  else if (layer === 'equals') {
    s = lookup.equals[String(code)];
  }
  else if (layer === 'above') {
    s = lookup.above[String(code)];
  }
  else {
    console.log("[!] Invalid layer")
    s = undefined;
  }

  // there can be various reasons for 's' being undefined => make behaviour consistent
  if (!s) {
    s = -1;
  }

  return s;
}


function make_tilemap() {
  // TODO: implement
}


console.log("[DBG] below: " + below);
console.log("[DBG] equals: " + equals);
