from django.contrib import admin

from game.models import Tileset, Level, Tilemap
# Register your models here.

myModules = [Tileset, Level, Tilemap]

admin.site.register(myModules)
