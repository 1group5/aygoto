from django.shortcuts import render
from django.views import generic
from django.http import HttpResponse

from .models import Level

# Hello world stuff
def home(request):
    return render(request, "proto/home.html")


def proto(request):
    return render(request, "proto/proto.html")


# DEPRECATED
def game(request):
    return render(request, "proto/game.html")


class LevelList(generic.ListView):
    """
    Generically list all the Level models
    """

    model = Level
    template_name = "proto/level_list.html"
    context_object_name = "levels"

    # def get_queryset(self):
    # return Level.objects.all()


class LevelDetail(generic.DetailView):

    model = Level
    template_name = "proto/level_detail.html"
    # slug_field = "uuid"
    # slug_field = "id"
    # slug_url_kwarg = "uuid"
    # slug_url_kwarg = "id"

    # def get_object(self, queryset=None):
    # print("[DBG] uuid-slug: {}".format(self.kwargs.get("uuid")))
    # return Ticket.objects.get(uuid=self.kwargs.get("uuid"))
