from django.urls import path

from game import views

urlpatterns = [
    path("", views.home, name="home"),
    path("levels/", views.LevelList.as_view(), name="levels"),
    path("level/<int:pk>/", views.LevelDetail.as_view(), name="level"),
]
