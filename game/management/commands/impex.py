"""
impex -- import/export util for Level data
"""

import djclick as click

from game.models import Level

import json


@click.command()
@click.option(
    "-i",
    "--import",
    "operation",
    flag_value="import_",
    default=True,
    help="import from a given FNAME (default)",
)
@click.option(
    "-o",
    "--export",
    "operation",
    flag_value="export_",
    help="dump to given FNAME",
)
@click.argument("fname")
def command(operation, fname):
    """
    Fully import/ export already prepared level data

    """
    # click.secho("[DBG] op: {}".format(operation))
    # click.secho("[DBG] fname: {}".format(fname), fg="red")

    if operation == "import_":
        click.secho("[*] importing from {}".format(fname), fg="white")

        try:
            with open(fname, "r") as f:
                imp = json.load(f)

                for i in imp:

                    matches = Level.objects.filter(name=i["name"])
                    matches_count = matches.count()

                    if matches_count == 0 or matches_count == 1:

                        lvl, created = Level.objects.get_or_create(
                            name=i["name"],
                            height=i["height"],
                            width=i["width"],
                        )

                        if created:
                            click.secho(
                                "[DBG] {} created.".format(lvl.name), fg="white"
                            )
                            lvl.data = i["data"]
                        else:
                            click.secho(
                                "[DBG] {} already present.".format(lvl.name), fg="blue"
                            )

                        lvl.save()

                    elif matches_count > 1:
                        click.secho(
                            "[*] {} already present.".format(i["name"]), fg="yellow"
                        )
                    else:
                        click.secho(
                            "[!] Error importing {} .".format(i["name"]), fg="red"
                        )

            click.secho("[=] done importing from {}.".format(fname), fg="green")

        except Exception as exc:
            click.secho("[!] ERROR: {}.".format(exc), fg="red")

    elif operation == "export_":
        click.secho("[*] exporting to {}...".format(fname), fg="white")
        export = [l.to_json() for l in Level.objects.all()]

        try:
            with open(fname, "w") as f:
                json.dump(export, f)
            click.secho("[=] written to {}.".format(fname), fg="green")
        except Exception as exc:
            click.secho("[!] ERROR: {}.".format(exc), fg="red")
