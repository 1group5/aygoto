from rest_framework import viewsets

from .models import Level
from .serializers import LevelSerializer


class LevelViewset(viewsets.ModelViewSet):
    queryset = Level.objects.all()
    serializer_class = LevelSerializer
