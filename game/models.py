from uuid import uuid4

from django.db import models

from aygoto.settings import ASSET_DIR

import csv
import json

# Create your models here.

# NOTE: maybe shorter id?

# Map


class Tilemap(models.Model):
    """

    DEPRECATED
    tie together Tileset and Level to create a complete map.

    """

    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=50)
    f = models.FileField(upload_to="uploads/levels/")
    level = models.ForeignKey("Level", on_delete=models.CASCADE)
    tileset = models.ForeignKey("Tileset", on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Tileset(models.Model):
    """
    The Tileset model.
    Responsible for holding an image and the necessary info to extract tiles from the set

    TODO: Tileset: identify single tiles out of tilemap image.
    NOTE: now uploads tilesets to the default asset dir location
    """

    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    name = models.CharField(max_length=50)
    f = models.FileField(upload_to=ASSET_DIR)
    height = models.IntegerField(default=0)
    width = models.IntegerField(default=0)

    def __str__(self):
        return self.name


# Level


class Level(models.Model):
    """

    level as plain data-Wrapper around 'data' JSON object that holds the actual level matrices

    :class: `aygoto.game.Level`
    """

    # id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    uuid = models.UUIDField(unique=True, default=uuid4, editable=False)
    name = models.CharField(max_length=50)
    data = models.JSONField(null=False, default=dict)
    f = models.FileField(upload_to="uploads/levels/")
    height = models.PositiveIntegerField(default=0)
    width = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.name

    def to_json(self):
        """
        export an existing Level instance to a consistent json object

        :return: the level-data with name, data, height and width keys
        :rtype: dict
        """
        out = {}

        out["name"] = self.name
        out["data"] = self.data
        out["height"] = self.height
        out["width"] = self.width

        return out

    def print_csv(self):
        """
        DEPRECATED

        simply dump the csvFile's contents
        """
        with open(self.f.path, "r") as csvFile:
            input_file = csv.DictReader(csvFile)

            for row in input_file:
                print(row)
