from django.core.files.base import ContentFile
from django.core.management import call_command

from django.db import models

import httpx
import os
from zipfile import ZipFile

# Create your models here.


class DataSource(models.Model):
    """
    Most basic wrapper around any types of data sources
    """

    location = models.CharField(max_length=240)

    class Meta:
        abstract = True


class Shapefile(models.Model):
    """
    Wrap around locally extracted .shp files
    """

    f = models.FileField(upload_to="data")


class Geofabrik(DataSource):
    """
    process data from Geofabrik.de

    https://download.geofabrik.de/



    example of a valid shape file:

    https://download.geofabrik.de/antarctica-latest-free.shp.zip
    """

    remote = models.URLField()
    f = models.FileField(upload_to="data")
    fetched = models.BooleanField(editable=False, default=False)
    extracted = models.BooleanField(editable=False, default=False)

    # def __init__(self):
    # self.location = self.remote

    def __str__(self):
        return str(self.remote)

    class Meta:
        verbose_name = "Geofabrik Source"
        verbose_name_plural = "Geofabrik Sources"

    def _dest(self):
        """
        construct the destination directory for use in 'extract' and 'enumerate'
        """
        data_dir = os.path.dirname(self.f.path)
        dirname = os.path.basename(
            os.path.splitext(os.path.splitext(self.f.path)[0])[0]
        )
        dest = os.path.join(data_dir, dirname)
        return dest

    # TODO:
    # 1. download
    def fetch(self):
        """
        Download the specified file and save it to the data dir
        """
        if not self.fetched:
            try:
                r = httpx.get(self.remote)

                fname = os.path.basename(self.remote)
                fcontent = ContentFile(r.content)

                self.f.save(fname, fcontent)

                self.fetched = True

                # TODO:
                # finish file fetching

                # something like:
                # new = File(r.content)
                # self.f.save('new_name', new)
                #
                # need to test this tomorrow on a decent internet connection

            except Exception as e:  # generally not so good to naively catch everything
                print(e)

    # https://download.geofabrik.de/europe/germany/nordrhein-westfalen/detmold-regbez-latest-free.shp.zip
    # 2. extract
    def extract(self):
        """
        Perform extraction on the local file
        TODO: look up how to extract arcihves in python
        """
        # something like:
        # wth zipfile.ZipFile(pathto_zipfile, 'r') as zip_ref:
        #      zip_ref.extractall(directyory_to_extract_to)
        dest = self._dest()

        if not os.path.exists(dest):
            os.mkdir(dest)

        if not self.extracted:
            with ZipFile(self.f.path, "r") as z:
                z.printdir()

                z.extractall(dest)

                self.extracted = True

    # 3. enumerate
    def enumerate(self):
        """
        Create objects of a very simple model to inform about the available files
        """
        dest = self._dest()

        for f in os.listdir(dest):
            if f.endswith(".shp"):
                print("[DBG] found shapefile: {}".format(f))

        # something like:
        # os.listdir(directory_to_extract_to)
        # identify .shp files

    # 4. ingest ( => might not be DataSource's responsibilty  )
    def ingest(self):
        """
        Ingest the contained .shp files into DB
        """
        dest = self._dest()

        for f in os.listdir(dest):
            if f.endswith(".shp"):
                print("[DBG] ingesting shapefile: {}".format(f))

                out = os.path.join(dest, f)

                for term in ["buildings", "water_a", "railways", "roads"]:
                    if term in f:
                        print("[DBG] [ingest] f: {}".format(out))
                        call_command("load", out)


class Overpass(DataSource):
    """
    process data via Overpass API

    TODO:

    add similar behaviour to Overpass-transmitted data
    """

    pass
