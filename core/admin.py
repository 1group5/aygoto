from django.contrib import admin

from core.models import Geofabrik, Shapefile

# Register your models here.

myModules = [Geofabrik, Shapefile]

admin.site.register(myModules)
