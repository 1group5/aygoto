THIS_FILE := $(lastword $(MAKEFILE_LIST))
DOCKER := "docker"
COMPOSE := "docker-compose"
#DOCKER := "podman"
#COMPOSE := "podman-compose"

.PHONY: help build up start down destroy stop restart logs logs-server ps client-shell server-shell db-shell

help:
	make -pRrq -f $(THIS_FILE) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ { if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

####################################################################################
### Generic docker handling
####################################################################################

build:
	${COMPOSE} -f docker-compose.yml build $(c)

up:
	${COMPOSE} -f docker-compose.yml up -d $(c)

start:
	${COMPOSE} -f docker-compose.yml start $(c)

down:
	${COMPOSE} -f docker-compose.yml down $(c)

destroy:
	${COMPOSE} -f docker-compose.yml down -v $(c)

stop:
	${COMPOSE} -f docker-compose.yml stop $(c)

restart:
	${COMPOSE} -f docker-compose.yml stop $(c)
	${COMPOSE} -f docker-compose.yml up -d $(c)

logs:
	${COMPOSE} -f docker-compose.yml logs --tail=100 -f $(c)

ps:
	${COMPOSE} -f docker-compose.yml ps

####################################################################################
### convenience/ aygoto-specifics
####################################################################################

web-shell:
	${COMPOSE} -f docker-compose.yml exec web /bin/bash

migrate:
	${COMPOSE} -f docker-compose.yml exec web /usr/local/bin/python3 manage.py makemigrations
	${COMPOSE} -f docker-compose.yml exec web /usr/local/bin/python3 manage.py migrate

genisis:
	${COMPOSE} -f docker-compose.yml exec web /usr/local/bin/python3 manage.py genisis

repl:
	${COMPOSE} -f docker-compose.yml exec web /usr/local/bin/python3 manage.py shell

ipython:
	${COMPOSE} -f docker-compose.yml exec web /usr/local/bin/python3 manage.py shell -i ipython

db-shell:
	${COMPOSE} -f docker-compose.yml exec db /bin/bash

####################################################################################
### main targets
####################################################################################

test:
	${COMPOSE} -f docker-compose.test.yml build
	${COMPOSE} -f docker-compose.test.yml exec web pytest

doc:
	${COMPOSE} -f docker-compose.doc.yml build
	${COMPOSE} -f docker-compose.doc.yml exec web /usr/bin/make html
	${COMPOSE} -f docker-compose.doc.yml exec web /usr/bin/make latexpdf
