Game
====


The actual game engine.

We use Phaser3 for this purpose as it integrates well with Django and is easy to use.



🌍

Here some docs:


Load the data and everything needed inside the game here:

.. js:autofunction:: preload

bootstrap everything up!

.. js:autofunction:: create


rum this every frame /  game tick, gets updated constantly.
register immediate actions as callbacks here.

.. js:autofunction:: update



Util Functions
..............

The util functions here are used to ease the actual construction of usable level data.


The look_mapping function is especially handy, as it allows mapping our server-response encoded JSON array with a `lookup.json` file, that correlates tileset IDs to the server-codes.
This way thet tileset can potentially be swopped out easily.


.. js:autofunction:: lookup_mapping
