Geo Data-Source
===============



Here, we list and discuss different data sources and their use to us.




Openstreetmap
-------------

OSM provides several ways to access their data.
They are as follows:


- `openstreetmap.org <https://openstreetmap.org/export>`_

  The default map. Let's you selectively choose an area and export it.

  This is mostly useful for developing or testing certain areas, without needing to download and update large datasets.


- `Overpass API <https://overpass-api.de/>`_

  High-Throughput API for dynamically querying OSM Objects in large numbers.

- `Planet OSM <https://planet.openstreetmap.org/>`_

  Holds weekly updated complete(!) datasets of the whole earth. (100G+ in size)



- `Geofabrik <https://download.geofabrik.de/>`_

  Provides special subsets of Planet OSM, that help if one's interested in a specific region.

  They are finely grained, eg. for Germany, one can choose between:

    - Continent (Europe)                             -- 23.7G
    - Nation (Germany)                               -- 3.4G
    - Federal State (NRW)                            -- 707M
    - Legislative Area (Regierungsbezirk Düsseldorf) -- 166M


    This makes it easy to integrate a whole area of interest, while under performance (CPU, Mem, Storage) constraints





Natural Earth
-------------


`naturalearthdata.com <http://www.naturalearthdata.com/downloads/>`_

They provide free vector and raster maps in the following scales:

- Large - 1:10m

- Medium - 1:50m

- Small - 1:110m




opentopography
--------------


`opentopography.org <https://opentopography.org/>`_


They provide free topographical (with height) maps.




ESRI Open Data Hub
------------------

`arcgis <http://opendata.arcgis.com/>`_

A collection and search-provider for maaany open data sources.
