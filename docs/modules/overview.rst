Overview
========

Realworld
#########

Realworld for representing the real OSM geometric objects

.. uml:: realworld
    :classes:
    :packages:

Gameworld
#########

Gameworld for mapping realworld to game data

.. uml:: gameworld
    :classes:
    :packages:

Game
####

Game for holding the completely processed data in JSON form

.. uml:: game
    :classes:
    :packages:
