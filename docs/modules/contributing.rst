Contributing
============

To start contributing to the aygoto project, do the following:


1) setup .gitconfig
2) `git clone https://gitlab.com/1group5/aygoto`
3) `git checkout -b dev`
4) `git pull origin dev`
5) now start the project and start changing files!
6) `git push origin dev`


Mind the following branch structure of the repo

* master -- latest stable
* stage -- staging
* dev  -- development branch


During code reviews, code gets improved and sinks down the following path:

.. code-block::

   dev --> stage --> master


The dev brach is where latest changes enter the project.

As features get completed, tested and accepted, they get merged into stage, the staging branch whose configuration is close to production.

Finally, as things get smoothed out and subtle bugs are catched, it will finally go into master.

Master aims to be the latest stable release of aygoto.


Mind that merging into a 'higher' branch is always accompanied by a review process.


Versioning
----------


Note that we stick to semantic versioning (SemVer).


You can read more about this `here <https://semver.org>`_.


As a quick rundown, the summary states the following:


.. code-block::

    Given a version number MAJOR.MINOR.PATCH, increment the:

    1. MAJOR version when you make incompatible API changes,
    2. MINOR version when you add functionality in a backwards compatible manner, and
    3. PATCH version when you make backwards compatible bug fixes.

    Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.






More precisely, for our projetct that means:

say, we're at **2.0.0**:


- fixing a bug, like a typo would increase to **2.0.1**         (patch)
- adding a new route to `urls.py` would increase to **2.1.0**   (new feature)
- changing the DB backend would increase to **3.0.0**           (breaking changes)




In addition to SemVer versioning, we use the python package `versioneer` to autogenerate the version from git tags, which introduces even more information. Let's break it down:


Say we have the following Release: `2.0.0+0.g0d7ea49.dirty`. It can be broken down to the following:


- `2.0.0`       - the semantic version described above.
- `+0`          - the number of builds for this version (usually gets incremented by CI server)
- `g0d7ea49`    - the current git commit
- `.dirty`      - this gets appended when there are untracked files in your repo. (=> repo is 'dirty')
