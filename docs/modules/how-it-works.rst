How it works
============


A starting Point
----------------

To get this whole machinery going, we construct a point from WGS84 Lat/Lon coordinates.
This point is then used as **centroid** for further construction, but the only information that really needs to be persisted.

.. image:: ../images/01_centroid.png

From the **centroid**, a **Chunk** (a square with the edge length of CHUNK_SIZE => defaults to 100 meters) is constructed.
This done by using GeoPy's distance function for metric distance/ coordinate conversion.
With the corners of the Chunk calculated, it is constructed by drawing a linestring around the point objects.
The chunk is the constructed, and its geometric entity will be stored to the DB.

.. image:: ../images/02_chunk.png

Once the Chunk is done, it fetches the **TL** (Top-Left) corner as coordinates from the Chunk, this point is (0, 0) for addressing Crumbs.
Crumbs are the smallest atom of map conversion. Each tile in the game corresponds to 1 sqm in the real world. A **Crumb** is the entity holding that relation and making this happen.


.. image:: ../images/03_crumbs.png

The current implementation is quite slow, but it is useful for debugging and tracking the progress on this long-lasting process of constructing a **Chunk** as well as its **Crumbs**.


.. image:: ../images/04_crumbs.png


You can slowly see the **Chunk** be filled more and more by **Crumb**s.

.. image:: ../images/05_crumbs.png


Until finished.

.. image:: ../images/06_crumbs.png



When all Crumbs are generated, the **Chunk** is ready to be used in LevelMapping construction.
A **LevelMapping** is a is an entity that holds the relation between a playable **Level** (with json data) and its corresponding **Chunk**.

It's perfectly fine to just already generated level data, without having the underlying **realworld**, **gameworld** data.

Level
-----

What the **LayerMapping** ultimately saves is an instance of the **game.Level** model. Its **data** field holds the actual map data in json format.
The components for now all expect this to work with the self-developed encoding of a having a flat dict with just the layers named and data as values.


The **Level** for now consists of 3 layers:

- below
- equals
- above

One for the ground and terrain, one for obstacles and on eye-level with the player, one for rooftops and clouds and the like.

.. code:: shell

   python manage.py impex <FNAME>
   python manage.py impex -i/--import <FNAME>
   python manage.py impex -o/--export <FNAME>


Allows to dump already generated data into a plain json file you can share with your friends!


How it works precisely
----------------------

+ lat,lon pair as input
+ Chunk gets constructed
+ Crumbs get created for Chunk
+ LevelMapping ties together Level and Chunk instances

First, we construct a tuple of Lat and Lon coordinates.

We use it it then to initialize and construct the Chunk via it's centroid.

A LevelMapping builds Level data out of a Chunk's geographic properties


.. graphviz::

   graph {
    "Lat-Lon Pair" -- "Chunk" -- "LevelMapping";
    "LevelMapping" -- "Chunk";
    "LevelMapping" -- "Level";
   }



When the centroid is set on the Chunk, it can be constructed.
"Constructed" meaning it caculatd the coordinates for its geometric object,
and saving after constructing is recommended.

First it reaches out CHUNK_SIZE/2 into all directions,
forming a cross made up of coordinate points

.. graphviz::

   digraph {
    "C" -> "N";
    "C" -> "E";
    "C" -> "S";
    "C" -> "W";
   }

From these points calculate the corners of the [TOP|BOTTOM][RIGHT|LEFT] points.

.. graphviz::

   digraph {
    "C" -> "N" -> "TL";
    "C" -> "E" -> "TR";
    "C" -> "S" -> "BL";
    "C" -> "W" -> "BR";
   }


having these processed, a LineString will be drawn around the Chunk and be saved as its geom Object.

When the Chunk's geom and object in general are saved, we can generate Crumbs.

`Chunk.gen_crumbs()` is the intended function for the user to create all Crumbs for a Chunk.

it will dynamically call `Chunk.tl()` to get the top-left or north-westernst Point of the Chunk and treat it as (x,y) coordinates (0,0).

.. graphviz::

   digraph {
    "C" -> "0:0";
    "C" -> "0:1";
    "C" -> "0:2";
    "C" -> "0:3";
    "C" -> "...";
    "C" -> "M:N";
    "C" -> "CHUNK_SIZE-1:CHUNK_SIZE-1";
   }

This is the most time-consuming part so far in the calculation, as it involes the construction and storage procedure for each Crumb.
Crumbs are CHUNK_SIZE**2 ( with a default begin CHUNK_SIZE=100, this makes 10000 Crumbs per Chunk! )

In the future this runtime penalties might be tackled with bulk inserts are parallelization/ job distribution


LevelMapping
------------

The LevelMapping ties together the Chunk and realworld data

Querying
........

querying is what happens next.

We favor the cost of an extra query to trim down the objects we have to act on.

We select the Crumbs of interest by merely checking their ForeighKey relation


Then we conduct spatial lookups on the Chunk and realworld data in order to trim down the vast amount of objects in the DB to the ones that are of interest to us.

below:

.. graphviz::

   digraph {
    "Water" -> "C.geom";
    "Road" -> "C.geom";
    "Railway" -> "C.geom";
   }

equals:

.. graphviz::

   digraph {
    "Building" -> "C.geom";
   }


this can be achieved by checking whether "C.geom" *covers* or *intersects* any of the realworld geometries,

LevelMapping that initializes a numpy zeroes array,
it tracks the intersection of Crumbs and marks their position with the corresponding code

below:

.. graphviz::

   digraph {
    "default/dirt" -> "0";
    "water" -> "1";
    "road" -> "2";
    "railway" -> "3";
   }

equals:

.. graphviz::

   digraph {
    "default/empty" -> "0";
    "building" -> "1";
   }

This enconding is deliberately decoupled from the client's interpretation of the data, as it might be desirable to change the Tileset oder data inerpretation altogether.

The resulting CHUNK_SIZE*CHUNK_SIZE arrays will then be embedded into te `game.models.Level.data` JSONField under the `layers.below` and  `layers.equals` keys.

The whole object is made accessible via DRF under the `/api/level/` key.

Level.data structure:

.. graphviz::

   graph {
    "layers" -- "below";
    "layers" -- "equals";
    "layers" -- "above";
   }



Data Formats
------------


Stressing that json output is possible at every processing level:

.. graphviz::

   digraph {
    "url" -> ".shp.zip" -> ".shp" -> "realworld" -> "gameworld" -> "game" -> "json";
    "realworld" -> "json";
    "gameworld" -> "json";
   }




at various stages in this process data can be accessed and managed as necessary through adiquate API endpoints.
Say, you could remotely manage realworld data on a aygoto instance to share an already ingested country.
