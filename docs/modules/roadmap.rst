Roadmap
=======

The aygoto development roadmap

TODO: we might need to classify features into the following categories:

#. Core Features (absolutely necessary for running anything)

#. Planned Features (stuff that sits atop the core-features and cushions

out the world) (3. Delayed Features (stuff that we want, might want, but
is not possible/ feasible to do right now)) 4. Wist-list (should we
arrive before time, we might also do those)

Feature List
------------

PLEASE feel free to add anything else on your mind!

Core
----

-  [ ] game: loads level data
-  [X] game: collisions - Jasmin
-  [ ] game: physics
-  [X] game: dynamic gamesize - Jasmin
-  [ ] game: can fetch data from backend
-  [ ] game: can construct dynamic game
-  [ ] game: can load materials
-  [ ] game: player input handled (touchscreen) - Jasmin
-  [ ] game: ingame-UI - Julia
-  [ ] game: state? save mechanism -> where is the state of the game
   stored?
-  [ ] game: creating player model - Julia
-  [ ] server: Tilemap, Tileset, Level, Material - Azad, Marvin
-  [X] server: Road - Azad
-  [X] server: River - Azad
-  [X] server: Lake - Azad
-  [ ] server: gunicorn - Marvin
-  [ ] server: nginx - Marvin
-  [ ] server: letsencrypt - Marvin
-  [ ] server: systemd-unitfile - Marvin
-  [ ] server: .deb pkg - Marvin
-  [ ] server: LICENSE (GPL, BSD) - Marvin
-  [ ] server: can construct Map from data - Azad
-  [X] docs: Logo -> Julia
-  [X] docs: powerpoint to hovercraft -> Julia
-  [X] server: GeoDjango Tutorial (WorldBorders)
-  [X] server: OSM Datasource for fetching
-  [X] server: DB sqlite -> postgres
-  [X] server: DB postgres -> postgis
-  [X] game: loads tilemap data (from csv) - Jasmin
-  [X] game: player input handled (keyboard) - Jasmin
-  [X] game: camera handling while moving across the map - Jasmin
-  [X] docs: Presentation #4 - Julia
-  [X] docs: Presentation #5 - Julia
-  [X] docs: Presentation #6 - Julia
-  [ ] docs: Presentation #7 - Julia
-  [X] game: fix walking animation - Julia
-  [X] game: load layers from multiple CSV/ JSON files - Jasmin
-  [ ] game: map transition/ loading map data/ Scenes - Jasmin
-  [ ] server: emulate Tiled JSON output by crafting it manually - Azad
-  [X] server: realworld: DataSource for fetching geo data - Marvin
-  [X] server: gameworld: Chunk/Crumb Method - Marvin
-  [X] server: realworld: API Endpoints for geo objects (like with
   worldborder) - Marvin
-  [X] server: realworld: fix genisis/ missing
   TM\ :sub:`WORLDBORDER`.shp files - Marvin

Planned
-------

-  [-] game: menus
-  [ ] game: items
-  [ ] game: pickups
-  [ ] game: NPCs?
-  [ ] game: can render map based on level data and materials
-  [ ] server: scheduling
-  [ ] server: WebSocket?
-  [-] server: inter-aygoto
-  [ ] server: functions also when offline
-  [ ] server: caching
-  [ ] server: Terrain (from landuse.shp)
-  [ ] server: docs: create UML diags and stuff
-  [ ] server: django-sphinx

Wish-list
---------

-  [ ] game: multiplayer?
-  [ ] game: controller support
-  [ ] game: pause function? (wishlist, bc not sure if necessary
   depending on rest of the game)
-  [ ] game: character select (-> suggests multiple characters, thus
   avatar customization, alternatively taken as save game selection
   which may not make sense with the way progression works)
-  [ ] game: Vehicles?
-  [ ] server: distributed Chunk/ Crumb => Celery - Marvin
-  [ ] server: caching of generated map data
-  [ ] server: Chunk metadata of neighbours

To be sorted
------------

-  [ ] server: testing
-  [-] server: Map
-  [X] server: REST-API
-  [X] server: OSM Datasource for fetching
-  game: CSS @media-queries for getting screen-size of the user ->
   rendering gamemap/ camera-scene accordingly

Timeline
--------

-  Jun

   -  25:
   -  26: Presentation: Concept

-  Jul

   -  27: sort out roadmap and starting tasks from it
   -  28: Finish assigned starting tasks (details to follow if tasks
      assigned)
   -  29: slight break due to vaccinations, family and health issues etc
      - mostly backend work by marvin and azad
   -  30: slight break due to vaccinations, family and health issues etc
      - mostly backend work by marvin and azad

-  Aug

   -  31: Requesting geo help
   -  32: backend proto issue fixed, can implement csvs now
   -  33: Proto loads data -> finishes task loads tilemap data (from
      csv)
   -  34: Goal: Sort camera, movement [and collisions], presentation
      basis

-  Sep

   -  35: final decision on what we need/want for presentation
   -  36: act on the above, bother köhn
   -  37: Presentation 2
   -  38: Bug cleanup & goal sorting & game size & design elements

-  Oct

   -  39: map file generation! - how will that work with collisions?
      File format? Property? May need to redo
   -  40: for presentation: logo, game sizing, hopefully map generation
      progress - if not implemented what else do we show?
   -  41: API endpoint for realworld/worldborder
   -  42: Presentation 3 - in bbb room test and prep
   -  43: Data Export (CSV, JSON)

-  Nov

   -  44: Chunk method implementation
   -  45: Docs workover ; presentation 4
   -  46: Crumb method (? => numpy -> lvl export)
   -  47: Scene Transistion; Level load from backend

-  Dec

   -  48: Ingame menus; UI
   -  49: Poster; Game Manual
   -  50: Data persistence => format to store and retrieve level data
      without geo-requirements
   -  51: Dynamic Level Data generation based on Tileset
   -  52: Final packaging, deployment

-  Jan

   -  1:
   -  2:
   -  3:
   -  4: development of impex utility to share already generated level
      data.
   -  5:

-  Feb

   -  6:
   -  7: simple level selection functionality
   -  8: dynamic rendering of level data possible

-  Mar

   -  9: documentation overhaul
