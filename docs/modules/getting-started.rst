Getting Started
===============


Welcome to aygoto!

This a project aiming to regenerate game maps based on GeoData.


Quickstart
----------

Provided you have *docker*, *docker-compose* and *gnumake* installed:

.. code:: sh

   make up
   make migrate
   make restart
   make genisis


This will build from source, spin up a PostGIS + the GeoDjango app, install the deps, migrates and fills the DB with WorldBorder data.

Developing Locally
------------------

You will likely want to use a local DB also. This can be achieved easily by doing: `export DB_HOST=localhost`.

To get started:
...............

.. code:: sh

          git clone https://gitlab.com/1group5/aygoto
          cd aygoto
          pipenv shell
          pipenv install
          pipenv manage.py migrate
          pipenv manage.py createsuperuser
          pipenv manage.py runserver




To spin up a local dev environment:
...................................

.. code:: sh

          git clone https://gitlab.com/1group5/aygoto
          cd aygoto
          pipenv shell
          pipenv install --dev
          pipenv manage.py migrate
          pipenv manage.py createsuperuser
          pipenv manage.py runserver
          # for doc generation:
          npm i -g jsdoc


Build docs
..........

.. code:: sh

          cd docs
          make html
