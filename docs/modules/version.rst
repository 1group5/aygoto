Version Check
=============


python3

.. program-output:: python3 --version || echo "{!] python3 not found"
  :shell:

pip3

.. program-output:: pip3 --version || echo "{!] pip3 not found"
  :shell:

pipenv

.. program-output:: pipenv --version || echo "{!] pipenv not found"
  :shell:

django-admin

.. program-output:: django-admin --version || echo "{!] djago-admin not found"
  :shell:

sphinx-build

.. program-output:: sphinx-build --version || echo "{!] sphinx-build not found"
  :shell:

pytest

.. program-output:: cd .. && pytest --version
  :shell:

psql

.. program-output:: psql --version || echo "{!] psql not found"
  :shell:

docker

.. program-output:: docker --version || echo "{!] docker not found"
  :shell:

docker-compose

.. program-output:: docker-compose --version || echo "[!] docker-compose not found"
  :shell:

GeoPandas

.. program-output:: python3 -c "import geopandas; geopandas.show_versions()" || echo "[!] geopandas not found"
  :shell:
