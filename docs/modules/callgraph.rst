Callgraph
=========

Realworld
#########

Realworld is responsible for storing the real world (OSM) data inside Django and make it queryable.

A whole lot of geographic data.


From OSM .shp files (preprocessed by geofabrik.de), we store the following objects:

- WorldBorder
- Water
- Road
- Rail
- Building

(+ many more, but those are currently in uses)

.. callgraph:: realworld
    :direction: horizontal

Gameworld
#########

Gameworld is responsibel for establishing a mapping between realworld and game data. It is capable of creating game data from realworld data.

.. callgraph:: gameworld
    :direct

Data types:

- Chunk 🌐
- Crumb 🟩

Game
####

Game data is really dumb 2D JS arrays that store the ever encodings for either:

- Water 🌊
- Road 🛣
- Rail 🛤
- Building 🏘

.. callgraph:: game
    :direction: horizontal
