Architecture
============


How is this project organized?



Backend:

* Django

  The Python Web Framework named after a jazz musician.

* GeoDjango

  Special Version of Django that requires extra deps like GDAL, proj and GEOS.
  It extends the default django ORM and utilities with Geo-Functions and Models, that allow
  calculations on GIS data, e.g. `area()`, `distance()` and `contains()`.
  Plus it has convenience features for inspecting and importing foreign spatial data.

* DRF

  the django rest framework is a django application that aids in creating API endpoints rapidly and securely.
  It does so by providing serizaliers to Models that in turn can (de)serialize the Model's data into JSON, YAML, CSV or whatever your heart desires most.


* PostgreSQL

  The recommended DB backend for django in production, they go together like ebony and ivory.
  It's FOSS and has some features that other RDBMSes are lacking, like `Json` or `Binary` data types, Materialized Views, Foreign Data Wrappers, to name a few.

* Postgis

  At its core only a postgresql extension (speaking: plain SQL), but it offers a variety of features for the DB
  side of things, namely allowing to store GIS data `Point`, `Polygon` or `MultiPolygon` and do calculations on  this data.


* Geopy

  Also we use geopy for geodesic distance calculation, as it implements Vincenty's formulae quite well. See https://en.wikipedia.org/wiki/Vincenty%27s_formulae to learn more information.

  NOTE: at the time of this writing (Sun Nov  7 22:27:51 2021) with v2.2.0 of geopy, Vincenty distance is no longer availble.
  Geodesic distance calculation is now done via Karney's method.


Frontend:

* phaser3

  A JavaScript Game engine that is geared towards developing browser (or mobile web) games.
  Probably most famous for powering the Facebook Game API.
  It's simplistic, has a dedicated Arcade Physics Engine and is exactly what we need here.


NOTE: the idea here is to integrate phaser3 deeply into django and its template system, but ultimately aiming to let DRF negotiate the data handling in between. Thus might the diagram look like phaser3 could 'bypass' the API, but it is intended.
