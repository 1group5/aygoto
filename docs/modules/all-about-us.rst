All about us
============

We dev:


+ Jasmin Breeman 🕹

    * mail: jasmin.breemann@stud.hs-bochum.de
    * **aygoto game master**
    * will code for food - potatoes love is real
    * will regularly loose hours of time to typos

* Julia Biehl 🎨

    * mail: julia.biehl@stud.hs-bochum.de
    * **ministry of fine arts and aesthetics**

* Raber Amin ⚙️

    * mail: raber.amin@stud.hs-bochum.de
    * **backend linebacker**
    * same ride as Sarkozy
    * breaks your code, but fixes your car

* Marvin Evers 🧩

    * mail: marvin.evers@stud.hs-bochum.de
    * **aygoto's Mercator**
    * "Good day sir. Do you have a moment to talk about our lord and savior Richard Stallman?"
