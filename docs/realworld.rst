realworld package
=================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   realworld.migrations

Submodules
----------

realworld.admin module
----------------------

.. automodule:: realworld.admin
   :members:
   :undoc-members:
   :show-inheritance:

realworld.apps module
---------------------

.. automodule:: realworld.apps
   :members:
   :undoc-members:
   :show-inheritance:

realworld.models module
-----------------------

.. automodule:: realworld.models
   :members:
   :undoc-members:
   :show-inheritance:

realworld.tests module
----------------------

.. automodule:: realworld.tests
   :members:
   :undoc-members:
   :show-inheritance:

realworld.urls module
---------------------

.. automodule:: realworld.urls
   :members:
   :undoc-members:
   :show-inheritance:

realworld.views module
----------------------

.. automodule:: realworld.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: realworld
   :members:
   :undoc-members:
   :show-inheritance:
