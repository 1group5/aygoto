# hello, am i a comment?

:title: Aygoto
:author: Jasmin, Julia, Azad, Marvin
:describtion: Dritte Präsentation

:css: style.css

.. header::

    .. image:: images/map.png

	
.. footer::

  Aygoto von: Jasmin, Julia, Azad, Marvin
   
----

 
Aygoto
================

----


Gliederung
------------

* Pitch
* Aktueller Stand Frontend
* Map Generierung
	* Idee
	* Umsetzung
	* Interessanter Code
	*Aktueller Stand
* Ziele

----

Pitch
-----
    * Entdecker Spiel, welches aus realer Welt generiert wird
    * Erkunde ganz neue Seiten deiner Stadt!
	* Fokus auf Map und nicht auf spielerischer Seite

----


Aktueller Stand - Frontend
---------------

* Spieler hinzugefügt

* Präsentation des Spiels

----

Interessanter Code - Frontend
------------------------------

   .. code:: javascript
   
    camera.setBounds(0,0,1280,1280);
	
        this.anims.create({
      key: "left",
      frameRate: 4,
      frames: this.anims.generateFrameNumbers("charakter", {start: 3, end:5}),
      repeat: -1
    });

	if(Phaser.Input.Keyboard.JustDown(leftKey)){
  		this.player.anims.play("left");

----

Map Generierung - Idee
------------------------------

  

----

Map Generierung - Umsetzung
------------------------------



----

Map Generierung - Interessanter Code
---------------------------------------

   .. code:: python
	
	worldborder_mapping = {
		'fips': 'FIPS',
		'iso2': 'ISO2',
		'iso3': 'ISO3',
		'un': 'UN',
		'name': 'NAME',
		'area': 'AREA',
		'pop2005': 'POP2005',
		'region': 'REGION',
		'subregion': 'SUBREGION',
		'lon': 'LON',
		'lat': 'LAT',
		'mpoly': 'MULTIPOLYGON',
	}

----

Map Generierung - Aktueller Stand
------------------------------



----

Ziele
-------------

.. image:: images/Ziele.png

----

Sources
-------

* https://opengameart.org/sites/default/files/MAP.png
* https://opengameart.org/sites/default/files/32x32_map_tile%20v3.1_0.png


Sources
-------

www.de
hello
