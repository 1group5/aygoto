# hello, am i a comment?

:title: Aygoto
:author: Jasmin, Julia, Azad, Marvin
:describtion: Vierte Präsentation

:css: style.css

.. header::

    .. image:: images/Logo.jpg

	
.. footer::

  Aygoto von: Jasmin, Julia, Azad, Marvin
   
----

 
Aygoto
================

----


Gliederung
------------

* Technische Details

	* Software
	* Formate
* Zeitplan
* Aktueller Stand
* Interessanter Code

----


Technische Details
------------------

* Architektur

.. image:: images/Architektur.png

----

Software
-----------
.. image:: images/Software.png

----

Formate
---------

- .csv
- .json
- .shp

----

Zeitplan
------------

.. image:: images/Timeline.png

----


Aktueller Stand
---------------

.. image:: images/Chunk.png

----

Interessanter Code -- Chunk
---------------------------

.. code:: python

   from aygoto.gameworld import Chunk

   p = (6.781115,51.229276) # lon, lat
   c = Chunk()
   c.set_centroid(p)
   c.construct()


----

Interessanter Code -- spatial lookups
-------------------------------------

.. code:: python

   from aygoto.realworld import Road, Building, Railway

   intersect_buildings = Building.objects.filter(geom__intersects=c.geom)  # 2 of 2118527
   contained_buildings = Building.objects.filter(geom__contained=c.geom)



----


Sources
-------

* https://opengameart.org/sites/default/files/MAP.png
* https://opengameart.org/sites/default/files/32x32_map_tile%20v3.1_0.png
* https://indigo.amsterdam/wp-content/uploads/2017/05/python-django-logo-1024x576.jpg
* https://day-journal.com/memo/images/logo/geodjango.png
* https://geopy.readthedocs.io/en/stable/_images/logo-wide.png
* https://www.osgeo.org/wp-content/uploads/postgis-logo-1.png
* https://res.cloudinary.com/practicaldev/image/fetch/s--dhdwfcO7--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/auos4rri1iucy5d1p0bw.png
* https://th.bing.com/th/id/R.ff8311dade09eb45fc2454e8fccd1fc7?rik=RQRWcT3FCgdzBQ&riu=http%3a%2f%2fwiseape-technologies.com%2fwp-content%2fuploads%2f2020%2f12%2fpostgresql-logo.png&ehk=6Z2idCR8WTybqlSVki%2bPuxeWdfxi660QNhD5ZeCwUkI%3d&risl=&pid=ImgRaw&r=0
* https://haozhang95.github.io/Python24/19Django%E6%A1%86%E6%9E%B6%E5%A2%9E%E5%BC%BA/Django_Rest_Framework/images/drf_logo.png
