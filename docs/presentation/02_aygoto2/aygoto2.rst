# hello, am i a comment?

:title: Aygoto
:author: Jasmin, Julia, Azad, Marvin
:describtion: Zweite Präsentation

:css: style.css

.. header::

    .. image:: images/map.png

	
.. footer::

  Aygoto von: Jasmin, Julia, Azad, Marvin
   
----

 
Aygoto
================

----


Gliederung
------------

* Pitch
* Technische Details
* Organisation
* Aktueller Stand
* Interessanter Code
* Ziele

----

Pitch
-----
    * Entdecker Spiel, welches aus realer Welt generiert wird
    * Erkunde ganz neue Seiten deiner Stadt!

----


Technische Details
------------------

* Architektur

.. image:: images/Architektur.png

----

Organisation
------------

* Roadmap

.. image:: images/Roadmap.png

----


Aktueller Stand
---------------

* Präsentation des Spiels

----

Interessanter Code - Frontend
------------------------------

   .. code:: javascript
   
    camera.setBounds(0,0,1280,1280);
	
        this.player = this.physics.add.sprite(0, 0, 'bomb');
        this.player.setCollideWorldBounds(true);
        camera.startFollow(this.player, true);

    this.player.setVelocity(0);
        if (cursors.up.isDown){
            this.player.setVelocityY(-300);
        }
        else if (cursors.down.isDown){
            this.player.setVelocityY(300);
        }
        if (cursors.left.isDown){
            this.player.setVelocityX(-300);
        }
        else if (cursors.right.isDown){
            this.player.setVelocityX(300);
        }

----

Interessanter Code - Backend
------------------------------

   .. code:: python

	class Roads(models.Model):
		osm_id = models.CharField(max_length=10)
		code = models.IntegerField()
		fclass = models.CharField(max_length=28)
		name = models.CharField(max_length=100,null=True)
		ref = models.CharField(max_length=20,null=True)
		oneway = models.CharField(max_length=1)
		maxspeed = models.IntegerField()
		layer = models.BigIntegerField()
		bridge = models.CharField(max_length=1)
		tunnel = models.CharField(max_length=1)
		geom = models.MultiLineStringField(srid=25832)

    roads_mapping = {
		'osm_id': 'osm_id',
		'code': 'code',
		'fclass': 'fclass',
		'name': 'name',
		'ref': 'ref',
		'oneway': 'oneway',
		'maxspeed': 'maxspeed',
		'layer': 'layer',
		'bridge': 'bridge',
		'tunnel': 'tunnel',
		'geom': 'MULTILINESTRING',
    }

----

Interessanter Code - Backend
------------------------------

   .. code:: python
   
	class WorldBorder(models.Model):
    
		# just the usual pure-django model fields:
		fips = models.CharField(max_length=2, null=True, default=None)
		iso2 = models.CharField(max_length=2)
		iso3 = models.CharField(max_length=3)
		un = models.IntegerField()
		name = models.CharField(max_length=50)
		area = models.IntegerField()
		pop2005 = models.BigIntegerField()
		region = models.IntegerField()
		subregion = models.IntegerField()
		lon = models.FloatField()
		lat = models.FloatField()
		# GeoDjango-specific field
		mpoly = models.MultiPolygonField(srid=4326)

		def __str__(self):
			return self.name

----

Interessanter Code - Backend
------------------------------

   .. code:: python
	
	worldborder_mapping = {
		'fips': 'FIPS',
		'iso2': 'ISO2',
		'iso3': 'ISO3',
		'un': 'UN',
		'name': 'NAME',
		'area': 'AREA',
		'pop2005': 'POP2005',
		'region': 'REGION',
		'subregion': 'SUBREGION',
		'lon': 'LON',
		'lat': 'LAT',
		'mpoly': 'MULTIPOLYGON',
	}

----

Ziele
-------------

.. image:: images/Ziele.png

----

Sources
-------

* https://opengameart.org/sites/default/files/MAP.png
* https://opengameart.org/sites/default/files/32x32_map_tile%20v3.1_0.png


Sources
-------

www.de
hello
