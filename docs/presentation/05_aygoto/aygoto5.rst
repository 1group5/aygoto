# hello, am i a comment?

:title: Aygoto
:author: Jasmin, Julia, Azad, Marvin
:describtion: Fünfte Präsentation

:css: style.css

.. header::

    .. image:: images/Logo.jpg


.. footer::

  Aygoto von: Jasmin, Julia, Azad, Marvin

----


Aygoto
================

----

:data-x: r2000

Gliederung
------------

* Über uns
* Die Idee
* Grundlagen und erste Schritte/Protoyp
  - Components (phaser3, js)
  - HTML Template Source, aygoto.js
  - Overview/ How it works
  - Snippets
* Einbinden von Geodaten
  - Components (GeoDjango, postGIS)
  - Chunk/ Crumb (method, diagram)
  - Overview/ How it works
  - Snippets
* Architektur
  - Overview
  - Django
  - Docker
  - Sphinx
* Organisation
  - Meetings
  - Kommunikation
  - Workflow (git, anydesk, trello, ...)
* Demonstration
  - actual gameplay
  - Sprites/ Tilesets/ Player aesthetics
* Sources

----

:data-x: r2000

Über uns
------------------

* 1group5 bestehend aus:

  * Jasmin Breemann
    * aygoto game master
  * Julia Biehl
    * ministry of fine arts and aesthetics
  * Azad Amin
    * backend linebacker
  * Marvin Evers
    * aygoto's Mercator

* InformatikstudentInnen an der Hochschule Bochum

----

:data-x: r2000

Die Idee
------------------

* Ursprunglich Game Dev Projekt

    * Erkundungsspiel
    * Ähnlich Zelda
* Interesse an Karten aus der echten Welt

    * Generierung dieser in den Fokus gerückt
    * Spielelemente in den Hintergrund gerückt

----

:data-x: r2000

Spielkonzept
------------------

* Erlebnis:

  * Entdecken der Spielwelt
  * Neue Orte kennenlernen
  * Mögliche Anpassung zu Tourismus- oder Bildungszwecken

* Genre:

  * Top-Down, Erkundung

* Avatar:

  * Vorgegebener Avatar, welcher vom Spieler gesteuert wird

* Steuerung und Spielbarkeit:

  * Einfache Steuerung durch Pfeiltasten

----

:data-x: r2000

Spielkonzept
------------------

* Welt:

  * 2D
  * Verschiedene Strukturen
  * Begrenzung der Spielwelt

* Atmosphäre:

  * Entspannte Musik, stressfreie Erkundung

----

:data-x: r2000

Grundlagen und erste Schritte
------------------------------

* Frontend
* Gestaltung
* Backend

----

:data-x: r2000

Frontend
-----------

* Phaser 3 HTML5 Framework

  * Physics
  * Animation
  * Input handling

----

:data-x: r2000

Gestaltung
-----------

* 2D Topdown im Pixel Look

* Tiled

  * erstellen von Test Maps

* Tileset von OpenGameArt

----

:data-x: r2000

Backend
-----------

* Django

  * Warum Django?

    * ORM
    * DB Migration
    * Admin bereich
    * standartmäßig sicher
    * Batteries included
    * DRF



* Postgres oder Postgis?

    * Unterschiede:

      * Postgres = Objekt-Relationale Datenbank
      * Postgis  = Pg Verbesserung der räumlichen Berechnungen

----

:data-x: r2000

Einbindung von Geodaten
-----------------------


.. image:: images/aygoto_Screenshot.png
    :height: 800px

Einbindung von Geodaten
-----------------------


.. image:: images/aygoto_screenshot2.png
    :height: 800px
*
*

----

:data-x: r2000

Architektur
===========
* over_view

  * hier ein diagramm

    * kurze erklärung warum  Django. (vorteile nachteile)

* diagramm wo/wie docker mitspielt.

  * erklärung warum docker.

* diagramm zu sphinx?

  * vorteile der automatieseirten doc erstellung.

* evtl warum postgres?

====


Organisation
============

* Kommunikation

* Produktivität

* Regelmäßige Präsentationen

----

:data-x: r2000

Kommunikation
-------------

* Telegram

  * Textkommunikation
  * Repository Benachrichtigungen

* Discord

  * Wöchentliche Treffen

.. image:: images/telegramlogo.jpg
    :width: 400
.. image:: images/discordlogo.jpg
    :width: 400

----

:data-x: r2000

Produktivität
-------------

* GitLab

  * Versionsverwaltung

* Trello

  * Tasks

* Roadmap

  * Zeitplanung

* Volle Dokumentation als PDF verfügbar

.. image:: images/gitlablogo.jpg
    :width: 400
.. image:: images/trellologo.png
    :width: 400

----

:data-x: r2000

Präsentationen
--------------

* BBB
* Hovercraft! für html5 Präsentationen

----

:data-x: r2000

Demonstration
---------------



----

:data-x: r2000

Interessanter Code -- Chunk
---------------------------

:data-x: r2000

.. code:: python

   from aygoto.gameworld import Chunk

   p = (6.781115,51.229276) # lon, lat
   c = Chunk()
   c.set_centroid(p)
   c.construct()



----

:data-x: r2000

Sources
-------

* https://opengameart.org/sites/default/files/MAP.png
* https://opengameart.org/sites/default/files/32x32_map_tile%20v3.1_0.png
* https://indigo.amsterdam/wp-content/uploads/2017/05/python-django-logo-1024x576.jpg
* https://day-journal.com/memo/images/logo/geodjango.png
* https://geopy.readthedocs.io/en/stable/_images/logo-wide.png
* https://www.osgeo.org/wp-content/uploads/postgis-logo-1.png
* https://res.cloudinary.com/practicaldev/image/fetch/s--dhdwfcO7--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/auos4rri1iucy5d1p0bw.png
* https://th.bing.com/th/id/R.ff8311dade09eb45fc2454e8fccd1fc7?rik=RQRWcT3FCgdzBQ&riu=http%3a%2f%2fwiseape-technologies.com%2fwp-content%2fuploads%2f2020%2f12%2fpostgresql-logo.png&ehk=6Z2idCR8WTybqlSVki%2bPuxeWdfxi660QNhD5ZeCwUkI%3d&risl=&pid=ImgRaw&r=0
* https://haozhang95.github.io/Python24/19Django%E6%A1%86%E6%9E%B6%E5%A2%9E%E5%BC%BA/Django_Rest_Framework/images/drf_logo.png
