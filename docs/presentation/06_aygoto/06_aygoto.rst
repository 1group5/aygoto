
:title: Aygoto
:author: Jasmin Breemann, Julia Biehl, Azad Amin, Marvin Evers
:describtion: Abschluss Präsentation

:css: style.css

.. header::

    .. image:: images/logonew.png


.. footer::

  Aygoto von: Jasmin Breemann, Julia Biehl, Azad Amin, Marvin Evers

----


Aygoto
================


:data-x: r2000

----


Gliederung
------------
.. container:: twocol

   .. container:: leftside
     
    * Intro
    * Umsetzung
    * Demo
    * Code
    * Organisation
    * Vision
    * Quellen
    

   .. container:: rightside

    .. figure:: images/logonew.png

----

:data-x: r2000

Über uns:
----------

    * Jasmin Breemann 🕹
    * Julia Biehl  🎨
    * Azad Amin  ⚙️
    * Marvin Evers  🧩
 
----

:data-x: r2000

Idee
----
.. container:: twocol

  .. container:: leftside

    * Ursprünglich Game Dev Projekt
     
      * Erkundungsspiel
         
        * Zelda
        * Pokemon
        * GTA II

  .. container:: rightsideideenfindung1
     
    .. image:: images/map.png
      :width:  800
      :scale: 120

----

:data-x: r2000

Idee
----
.. container:: twocol

  .. container:: leftside

    * Interesse an Karten aus der echten Welt

      * Generierung dieser in den Fokus gerückt
      * Spielelemente in den Hintergrund gerückt
     
  
  .. container:: rightsideideenfindung1

    .. image:: images/gta2.jpg
      :width: 800
      :scale: 120    
      

----

:data-x: r2000

Idee
----

.. container:: twocol

   .. container:: leftside

    * Zentrale Idee

      * Echte Geodaten für die Erzeugung der Map

      * Reale Orte im Spiel entdecken

      * beliebige Orte auswählbar


   .. container:: rightside

    .. image:: images/hsbo.png
      :width: 1200
      :scale: 120%

----


:data-x: r2000

Konzept
-------
.. container:: leftside

  * Erlebnis:

    * Entdecken der Spielwelt
    * Neue Orte kennenlernen

  * Genre:

    * Top-Down, Erkundung

.. container:: rightside

  .. image:: images/ProtoScreenshot.png
    :width: 800

----

:data-x: r2000


Konzept
-------
.. container:: leftside

  * Avatar:

    * Vorgegebener Avatar

  * Steuerung

    * Steuerung durch Pfeiltasten

.. container:: char

  .. image:: images/charakter.png
    :width: 200


----

:data-x: r2000

Konzept
-------

* Welt:

  * 2D
  * Verschiedene Strukturen
  * Begrenzung der Spielwelt

* Atmosphäre:

  * Stressfreie Erkundung

----

:data-x: r2000

Geodaten
-------------------------

  - Components (GeoDjango, postGIS)
  - Chunk/ Crumb (method, diagram)
  - Overview/ How it works
  - Snippets

----

:data-x: r2000

Chunk
-----


.. image:: images/01_centroid.png
    :width: 600px

.. image:: images/02_chunk.png
    :width: 600px
  

----

:data-x: r2000

Crumb
-----

.. image:: images/03_crumbs.png
    :width: 300px

.. image:: images/04_crumbs.png
    :width: 300px

.. image:: images/05_crumbs.png
    :width: 300px

.. image:: images/06_crumbs.png
    :width: 300px
  

----

:data-x: r2000

Vergleich
---------

.. image:: images/00_GMAP.png
    :width: 400px

.. image:: images/00_OSM.png
    :width: 400px

.. image:: images/00_SAT.png
    :width: 400px

----

:data-x: r2000



Code
----

:data-x: r2000

.. code:: python

   from aygoto.gameworld import Chunk

   p = (6.781115,51.229276) # lon, lat
   c = Chunk()
   c.set_centroid(p)
   c.construct()



----

:data-x: r2000

Technologien
------------

* Frontend
* Gestaltung
* Backend
* Verwendete Dateiformate

----

:data-x: r2000

Frontend
--------
.. container:: twocol

   .. container:: leftside

    * Phaser 3 HTML5 Framework

      * Open Source
      * Gute Dokumentation
      * Viele Beispiele
      * Verschiedene Render Möglichkeiten
      * Sehr viele "Helferfunktionen"
      * Vollständig in JS

  .. container:: rightside

    .. figure:: images/phaser.jpg

----

:data-x: r2000

Frontend
--------
.. container:: leftside

  * Phaser 3 HTML5 Framework

    * Physics
  
      * Mehrere Engines

    * Animation

      * Einfache Einbindung

    * Input Verarbeitung

      * Vorgegebene Tastaturfunktionen
  
.. container:: rightside

  .. image:: images/phasertempl.png
    :width: 800
    

----

:data-x: r2000

Gestaltung
----------
.. container:: twocol

   .. container:: leftside

    * 2D Topdown im Pixel Look

    * Tiled

      * Erstellung von Test Maps

    * Tileset von OpenGameArt

  .. container:: rightside

     .. image:: images/32x32_tilemap.png
       :height: 500
       :align: right
       
----

:data-x: r2000

Backend
-------

.. image:: images/backend.jpg

----

:data-x: r2000

Django
------

 .. container:: leftside

  * Django

    * Python ORM
    * DRF
    * Sicher*
  
  
.. container:: rightside

  .. image:: images/djangoorm.png
    :width: 800
    :align: right

----

:data-x: r2000

Django
------

 .. container:: leftside

  * Django

    * Batteries included
    * Admin Bereich
    * Db Intergration
    * uvm. ab Werk
    
.. container:: rightside

  .. image:: images/djangoadmin.png
    :width: 800
    :align: right

----

:data-x: r2000

Django
------

 .. container:: leftside

  * Genutzt von z.B
   
    * Pinterest
    * Mozilla
    * Instagram

.. container:: rightside

  .. image:: images/mozilöl.png
    :width: 500
    :align: right
    
  

----

:data-x: r2000

Django
------

* Warum Geopy und Geodjango?

  * GeoDjango erweitert Django um:

    * die Verwendung von räumlichen DBMS wie Postgis

  * Geopy erleichtert Python Entwicklern das Arbeiten mit Koordinaten

----

:data-x: r2000

Datenbank
---------
.. container:: twocol

   .. container:: leftside

      * Warum Postgres

         * frei und quelloffen
         * vielseitig anwendbar
         * fast jede Hardware
         * fast jedes OS
         
  .. container:: rightside

     .. image:: images/shapefile_map.png
       :width: 500
       :align: right
     
----


:data-x: r2000

Datenbank
---------
.. container:: twocol

   .. container:: leftside

      * Postgis?
          
        * Postgis erweitert Psql 
  
          * Geografische Objekte
          * Geografische Funktionen
          * Erweiterung benötigt 
  
             * Multipolygon
             * Flächenberechnung
         
  .. container:: rightside

     .. image:: images/shapefile_map.png
       :width: 500
       :align: right
     
----


:data-x: r2000

Architektur
-----------


  * Docker: db und web

    * db
    
      * PostGIS container

    * web:

      * vollständige Entwicklungsumgebebung auf Basis von Debian (python3.8.5)

  * build/deployment automatisch


----

:data-x: r2000



Architektur
-----------

.. image:: images/Architektur.png

----

:data-x: r2000

Architektur
-----------

.. image:: images/docker2.drawio.png
    :width: 600px

----

:data-x: r2000



Dateiformate
------------

.. image:: images/geofabrik.png
    :width: 200px

.. image:: images/shapefile.jpg
    :width: 200px

.. image:: images/pbf_file_extension.webp
    :width: 200px

----


Dateiformate
------------

.. image:: images/data_formats.png
    :height: 200px


----


:data-x: r2000

Organisation
-------------
.. container:: twocol

  .. container:: leftside

    * Kommunikation

    * Produktivität

    * Regelmäßige Präsentationen

  .. container:: rightside

     .. figure:: images/produktivität.jpg

----

:data-x: r2000

Kommunikation
-------------

* Telegram

  * Textkommunikation
  * Repository Benachrichtigungen

* Discord

  * Wöchentliche Treffen



----

:data-x: r2000

Produktivität
-------------
.. container:: leftside

  * GitLab

    * Versionsverwaltung

  * Trello

    * Tasks

  * Roadmap

    * Zeitplanung

  * Volle Dokumentation als PDF verfügbar

.. container:: right

  .. image:: images/Trello.png
    :width: 800
    :align: right



----


:data-x: r2000

Präsentationen
--------------

* BBB
* Hovercraft! für HTML5 Präsentationen

----

:data-x: r2000

Demonstration
---------------
* Viel Vergnügen!

----

:data-x: r2000

Visionen
---------------

  * Mögliche Anpassung zu Tourismus- oder Bildungszwecken
  * Möglichkeit zur realen Weltkartengenerierung für Spieleentwickler

----

:data-x: r2000

Quellen
-------
.. container:: sources  
  
    * https://www.geofabrik.de
    * https://fileproinfo.com/file-type-images/pbf
    * https://gisresources.com/understanding-shapefile-shp-file-format/
    * https://opengameart.org/sites/default/files/MAP.png
    * https://opengameart.org/sites/default/files/32x32_map_tile%20v3.1_0.png
    * https://indigo.amsterdam/wp-content/uploads/2017/05/python-django-logo-1024x576.jpg
    * https://day-journal.com/memo/images/logo/geodjango.png
    * https://geopy.readthedocs.io/en/stable/_images/logo-wide.png
    * https://www.osgeo.org/wp-content/uploads/postgis-logo-1.png
    * https://res.cloudinary.com/practicaldev/image/fetch/s--dhdwfcO7--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://dev-to-uploads.s3.amazonaws.com/i/auos4rri1iucy5d1p0bw.png
    * https://th.bing.com/th/id/R.ff8311dade09eb45fc2454e8fccd1fc7?rik=RQRWcT3FCgdzBQ&riu=http%3a%2f%2fwiseape-technologies.com%2fwp-content%2fuploads%2f2020%2f12%2fpostgresql-logo.png&ehk=6Z2idCR8WTybqlSVki%2bPuxeWdfxi660QNhD5ZeCwUkI%3d&risl=&pid=ImgRaw&r=0
    * https://haozhang95.github.io/Python24/19Django%E6%A1%86%E6%9E%B6%E5%A2%9E%E5%BC%BA/Django_Rest_Framework/images/drf_logo.png  
    * https://hun.4meahc.com/grand-theft-auto-2-55044
    * https://www.google.com/maps/place/Hochschule+Bochum/@51.4476204,7.2707735,17z/data=!4m5!3m4!1s0x47b9202f7602ce8f:0x4e238c6a9ed916a6!8m2!3d51.447395!4d7.2714325
    * 

  
