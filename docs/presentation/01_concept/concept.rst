# hello, am i a comment?

.. title:: aygoto - concept

----

Aygoto - Concept
================

asdfasdfasdfasdfasdfasdf

+ Jasmin
+ Julia
+ Julien
+ Azad
+ Marvin


----

Gliederung
------------
* Pitch
* Spielekonzept
* Technische Details
* Organisation
* Prototyp
* Aktueller Stand
* Vision
----

Pitch
-----
* Entdecker Spiel, welches aus realer Welt generiert wird
    * Erkunde ganz neue Seiten deiner Stadt!
----

Spielekonzept
-------------
* Erlebnis
    * Entdecken der Spielwelt
    * Über Geschichte der Orte lernen
* Genre 
    * Top-Down RPG
* Ziele 
    * Lösen von kleineren Quests
* Welt 
    * 2D
    * Verschiedene Strukturen
    * Begrenzung der Spielwelt
* Atmosphäre 
    * Passende Musik und Sound Effekte
* Story und Inhalt 
    * Keine klassische Story
    * Hauptsächlich durch Aufgaben entdecken und ggf. lernen
* Steuerung und Spielbarkeit
    * Enfache Steuerung durch Pfeiltasten und Aktionstaste
* Charaktere 
    * NPCs vergeben Aufgaben
* Avatar 
    * Top Down Perspektive (zunächst nicht custom)
* Punkte und Belohnungen 
    * Aufgaben geben ortsabhängige Belohnungen
    * Durch Erkundung werden mehr Orte verfügbar
----

Technische Details
------------------
#insert pictures here
* Phaser
* Pyhton
* aseprite
----

Organisation
------------
#insert pictures here
* Trello
* Discord
* GitLab
----

Prototyp
--------
An dieser Stelle ein kurzer Schwenk zum Prototyp
----

Aktueller Stand
---------------
#insert pictures here - labert halt was rum, wenn er mehr hören will, kann er drauf ansprechen
----

Vision/Ziele
-------------

----


Sources
-------

www.de
hello
