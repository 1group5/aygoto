# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import django

sys.path.insert(0, os.path.abspath(".."))
import _version

os.environ["DJANGO_SETTINGS_MODULE"] = "aygoto.settings"
django.setup()


# -- Project information -----------------------------------------------------

project = "aygoto"
copyright = "2021, Jasmin Breemann, Julia Biehl, Azad Amin, Marvin Evers"
author = "Jasmin Breemann, Julia Biehl, Azad Amin, Marvin Evers"

# The full version, including alpha/beta/rc tags
release = _version.get_versions()["version"]


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    # internal
    "sphinx.ext.autodoc",
    "sphinx.ext.coverage",
    "sphinx.ext.napoleon",
    "sphinx.ext.graphviz",
    # 3rd party
    "pyan.sphinx",
    #'sphinxcontrib.blockdiag',
    "sphinxcontrib.programoutput",
    "sphinx_rtd_theme",
    "sphinx_git",
    "sphinx_js",
    "sphinx_pyreverse",  # Removed due to bugs
]

graphviz_output_format = "svg"

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store", "presentation"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"
latex_elements = {"extraclassoptions": "openany,oneside"}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["../static/img"]
html_logo = "../static/img/Logo.jpg"


# -- RTD configuration ---------------------------------------------------
master_doc = "index"

# -- sphinx_js configuration ---------------------------------------------------
js_source_path = "../game/static/js/"

# -- PDF output configuration ---------------------------------------------------
latex_logo = "../static/img/Logo.jpg"
