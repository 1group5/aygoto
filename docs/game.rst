game package
=============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   game.migrations

Submodules
----------

game.admin module
------------------

.. automodule:: game.admin
   :members:
   :undoc-members:
   :show-inheritance:

game.apps module
-----------------

.. automodule:: game.apps
   :members:
   :undoc-members:
   :show-inheritance:

game.models module
-------------------

.. automodule:: game.models
   :members:
   :undoc-members:
   :show-inheritance:


game.urls module
-----------------

.. automodule:: game.urls
   :members:
   :undoc-members:
   :show-inheritance:

game.views module
------------------

.. automodule:: game.views
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: game
   :members:
   :undoc-members:
   :show-inheritance:
