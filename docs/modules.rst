aygoto
======

.. toctree::
   :maxdepth: 4

   game.rst
   realworld.rst
   aygoto.rst
   manage.rst
   gameworld.rst