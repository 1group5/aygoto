.. aygoto documentation master file, created by
   sphinx-quickstart on Tue Jun 15 00:13:51 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to aygoto's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/getting-started.rst
   modules/architecture.rst
   modules/overview.rst
   modules/callgraph.rst
   modules/game.rst
   modules/all-about-us.rst
   modules/contributing.rst
   modules/geodata-source.rst
   modules/how-it-works.rst
   modules/version.rst
   modules/changelog.rst
   modules/roadmap.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`




.. toctree::
   :maxdepth: 2
   :caption: Aygoto Index

   modules.rst
