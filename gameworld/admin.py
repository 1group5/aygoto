from django.contrib import admin

from gameworld.models import Chunk, Crumb, LevelMapping

# Register your models here.

myModules = [Chunk, Crumb, LevelMapping]

admin.site.register(myModules)
