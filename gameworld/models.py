from geopy.distance import distance, Distance

import json
import numpy as np

from django.contrib.gis.db import models
from django.contrib.gis.geos import Point, LineString

from aygoto.settings import CHUNK_SIZE
from game.models import Level
from realworld.models import *


# Create your models here.

SQM_DIAG = 1.4142135623730951


class Chunk(models.Model):
   
    """
   
    A single Chunk of the map

    ::todo:: add default None value to geom
   
    """

    name = models.CharField(max_length=50)
    size = models.IntegerField(default=CHUNK_SIZE)
    centroid = models.PointField()
    geom = models.LineStringField()

    def __str__(self):
        return str(self.name)

    def set_centroid(self, coord):
        """
       
        This function is for convenience to ease the construction of a Chunk

        :param coord: tuple, holding the lat and lon coordinates of the point to initialize
       
       
        """
       
        point = Point(coord)
        self.centroid = point

    def construct(self):
       
        """
       
        construct the geom based on the centroid of the Chunk
       
        """
        # create Point from Centroid
        p = self.centroid.coords
        #   - distance calculation
        chunk_size = distance(meters=self.size)
        d = chunk_size / 2
        #   - build NESW-cross
        N = d.destination(p, bearing=0)
        E = d.destination(p, bearing=90)
        S = d.destination(p, bearing=180)
        W = d.destination(p, bearing=270)
        #   - construct TR, TL, BR, BL
        TL = d.destination(W, bearing=0)
        TR = d.destination(N, bearing=90)
        BR = d.destination(E, bearing=180)
        BL = d.destination(S, bearing=270)
        # calculate points that make up rectangle
        tl = (TL.latitude, TL.longitude)
        tr = (TR.latitude, TR.longitude)
        br = (BR.latitude, BR.longitude)
        bl = (BL.latitude, BL.longitude)

        self.geom = LineString([tl, tr, br, bl, tl])
        # split up into crumbs
        # create numpy array for storing crumb values

    def get_tl(self):
        p = self.centroid.coords
        chunk_size = distance(meters=self.size)
        d = chunk_size / 2
        W = d.destination(p, bearing=270)
        TL = d.destination(W, bearing=0)
        tl = (TL.latitude, TL.longitude)
        return tl

    def has_crumbs(self):
        """

        method for checking whether Crumbs are already present

        :return: whether the Crumbs for this Chunk are already generated.
        :rtype: bool

        """
        crumbs = Crumb.objects.filter(chunk=self)
        if len(crumbs) > 0:
            return True
        return False

    def gen_crumbs(self):
      
        """

        a method for constructing all the necessary Crumbs inside a Chunk


        How it works:

          (0,0)
         TL +----+----+----+----+----+----+----+----+----+---->    West (Bearing 90)
            |
            |
            +
            |
            |
            +         * (2,2)
            |
            |
            +                             * (3,7)
            |
            |
            +                   * (4,4)
            |
            |
            v

        South (Bearing 180)

        To generate Crumbs inside a Chunk we iterate over the indices,
        - we go x times crumb_size right(west)
        - we get a point with correct X value
        - we go y times crumb_size down(south) from that point
        - we receive the actual point that Crumb's top_left takes for construction

        """

        tl = self.get_tl()

        for x in range(self.size):
            for y in range(self.size):
                print(f"{x}:{y}")
                # getting the target point for top_left
                west = distance(meters=1 * x)
                south = distance(meters=1 * y)
                w = west.destination(tl, bearing=90)
                s = south.destination(w, bearing=180)
                p = (s.latitude, s.longitude)
                # print(f"[DBG] w: {w}\np: {p}")

                # Crumb creation
                c = Crumb()
                c.set_top_left(p)
                c.construct()
                c.chunk = self
                c.x = x
                c.y = y
                c.name = f"Crumb {x}:{y} of {c.chunk}"
                c.save()

    def del_crumbs(self):
      
        """

        delete all the Crumbs from a Chunk

        """
      
        crumbs = Crumb.objects.filter(chunk=self)

        for c in crumbs:
            c.delete()

        print("[=] Done deleting Crumbs.")


class Crumb(models.Model):
    
    """

    A single crumb - part of a chunk, maps a Game Tile with a realworld sqm location

    """

    name = models.CharField(max_length=50)
    top_left = models.PointField()
    geom = models.LineStringField()
    x = models.PositiveIntegerField(editable=False)
    y = models.PositiveIntegerField(editable=False)
    chunk = models.ForeignKey("Chunk", on_delete=models.CASCADE)

    def set_top_left(self, coord):
        """
        This function is for convenience to ease the construction of a Crumb

        :param coord: tuple, holding the lat and lon coordinates of the point to initialize
        """
        point = Point(coord)
        self.top_left = point

    def construct(self):
    
        """

        construct geom based on top_left Point of the Crumb

        How it works:


        TL +----+ TR
           |    |
           |    |
        BL +----+ BR

        - TL (top-left) is used as PointField to initialize the Crumb
        - BR is calculated by going SQM_DIAG (sqrt(2)) South-West
        - BL and TR are calculated from BR

        """

        p = self.top_left.coords
        crumb_size = distance(meters=1)
        crumb_diag = distance(meters=SQM_DIAG)

        BR = crumb_diag.destination(p, bearing=135)
        TR = crumb_size.destination(BR, bearing=0)
        BL = crumb_size.destination(BR, bearing=270)

        br = (BR.latitude, BR.longitude)
        tr = (TR.latitude, TR.longitude)
        bl = (BL.latitude, BL.longitude)

        self.geom = LineString([self.top_left, tr, br, bl, self.top_left])

    # construct from centroid  => might be too inaccurate to use the same method as for Chunk construction
    # create geom
    # check for containment/ intersection


class LevelMapping(models.Model):
    
    """

    gameworld.LevelMapping -- Mapping Tiles/Crumbs to generate persisted Level data

    Maps a game.Level instance to a Chunk.
    Populates the Level.data json field with the Intersects from Chunk/Crumb Method


    """

    name = models.CharField(max_length=50)
    width = models.PositiveIntegerField(default=CHUNK_SIZE)
    height = models.PositiveIntegerField(default=CHUNK_SIZE)
    chunk = models.ForeignKey("Chunk", on_delete=models.CASCADE)
    _below = None
    _equals = None
    # models.JSONField()

    def __str__(self):
        return str(self.name)

    def gen_lvl(self):

        # TODO: fix this shit...
        lvl = np.zeros((self.width, self.height))
        return lvl

    # TODO: split those to their own dedicated functions
    def _water(self):
    
        """

        Init the Water parts in the level

        lookup code draft:
        0 - empty
        1 - water

          For Waters it's important to check which:
             - Crumbs INTERSECT with the water-area
             - Crumbs are COVEREDBY the water-area

        """
    
        # lvl = self.gen_lvl()
        # print("[DBG] empty lvl generated")

        # TODO:
        # - get Chunk
        chunk = self.chunk
        # - Get Crumbs contained by Chunk
        crumbs = Crumb.objects.filter(chunk=self.chunk)
        print("got target crumbs")
        # - Get Water contained by Chunk
        water_contained = Water.objects.filter(geom__contained=chunk.geom)
        water_intersects = Water.objects.filter(geom__intersects=chunk.geom)
        water = water_contained | water_intersects
        print("[*] got {} Waters total inside the Chunk".format(water.count()))

        # - Match all of them against Crumbs
        # and add it for the others
        for w in water:
            intersects = crumbs.filter(geom__intersects=w.geom)
            # FIXME this targets all the Crumbs ins the Chunk
            contained = crumbs.filter(geom__coveredby=w.geom)
            count = intersects.count()
            count2 = contained.count()
            print("[DBG] {} - intersects {} Crumbs".format(w.name, count))
            print("[DBG] {} - contained {} Crumbs".format(w.name, count2))

            # NOTE: instead try contains and coveredby lookups

            for crumb in intersects:
                # print("[DBG] intersection: x: {}| y: {}".format(crumb.x, crumb.y))
                self._below[crumb.x, crumb.y] = 1
            for crumb in contained:
                # print("[DBG] contained: x: {}| y: {}".format(crumb.x, crumb.y))
                self._below[crumb.x, crumb.y] = 1

    def _road(self):
    
        """

        return the below part of the map

        lookup code draft:
        0 - empty
        1 - water
        2 - road
        3 - railway

        - Roads and Railways are stored as multilinestrings
          For (roads|railways) it's important to check which:
             - Crumbs INTERSECT with the (roads|railways)-linestring
             - Crumbs CROSS the (roads|railways)-linestring
             #- Crumbs "TOUCH" the (roads|railways)-linestring
            => just a thought: is there a spatial lookup functions that checks whtether the geometries barely touch? or is this handled by INTERSECT?


        """

        # TODO:
        # - get Chunk
        chunk = self.chunk
        # - Get Crumbs contained by Chunk
        crumbs = Crumb.objects.filter(chunk=self.chunk)
        print("got target crumbs")

        # - Get Road contained by Chunk
        road_contained = Road.objects.filter(geom__contained=chunk.geom)
        road_intersects = Road.objects.filter(geom__intersects=chunk.geom)
        road = road_contained | road_intersects
        print("[*] got {} Roads total inside the Chunk".format(road.count()))
        for r in road:
            intersects = crumbs.filter(geom__intersects=r.geom)

            count = intersects.count()
            print("[DBG] {} intersects {} Crumbs".format(r.name, count))

            for crumb in intersects:
                # print("[DBG] intersection: x: {}| y: {}".format(crumb.x, crumb.y))
                self._below[crumb.x, crumb.y] = 2

    def _railway(self):
        """

        return the below part of the map

        lookup code draft:
        0 - empty
        1 - water
        2 - road
        3 - railway

        - Roads and Railways are stored as multilinestrings
          For (roads|railways) it's important to check which:
             - Crumbs INTERSECT with the (roads|railways)-linestring
             - Crumbs CROSS the (roads|railways)-linestring
             #- Crumbs "TOUCH" the (roads|railways)-linestring
            => just a thought: is there a spatial lookup functions that checks whtether the geometries barely touch? or is this handled by INTERSECT?


        """
    
        # - get Chunk
        chunk = self.chunk
        # - Get Crumbs contained by Chunk
        crumbs = Crumb.objects.filter(chunk=self.chunk)
        print("got target crumbs")

        # Get Railway contained by Chunk
        railway_contained = Railway.objects.filter(geom__contained=chunk.geom)
        railway_intersects = Railway.objects.filter(geom__intersects=chunk.geom)
        railway = railway_contained | railway_intersects
        print("[*] got {} Railways total inside the Chunk".format(railway.count()))

        for r in railway:
            intersects = crumbs.filter(geom__intersects=r.geom)

            count = intersects.count()
            print("[DBG] {} intersects {} Crumbs".format(r.name, count))

            for crumb in intersects:
                # print("[DBG] intersection: x: {}| y: {}".format(crumb.x, crumb.y))
                self._below[crumb.x, crumb.y] = 3

    def _building(self):
    
        """

        return the equals partt of the map

        .. note:: buildings should be trreated as mpolys (this goes analogue to water)


        lookup code draft:
        0 - empty
        1 - building

        """
    
        chunk = self.chunk
        crumbs = Crumb.objects.filter(chunk=self.chunk)
        print("got target crumbs")

        # Get Buildings contained by Chunk
        building_contained = Building.objects.filter(geom__contained=chunk.geom)
        building_intersects = Building.objects.filter(geom__intersects=chunk.geom)
        building = building_contained | building_intersects
        print("[*] got {} Buildings total inside the Chunk".format(building.count()))

        for b in building:
            intersects = crumbs.filter(geom__intersects=b.geom)
            contained = crumbs.filter(geom__coveredby=b.geom)
            count = intersects.count()
            count2 = contained.count()
            print("[DBG] {} - intersects {} Crumbs".format(b.name, count))
            print("[DBG] {} - contained {} Crumbs".format(b.name, count2))

            for crumb in intersects:
                # print("[DBG] intersection: x: {}| y: {}".format(crumb.x, crumb.y))
                self._equals[crumb.x, crumb.y] = 1
            for crumb in contained:
                # print("[DBG] contained: x: {}| y: {}".format(crumb.x, crumb.y))
                self._equals[crumb.x, crumb.y] = 1

    def below(self):
    
        """

        return the below part of the map

        lookup code draft:
        0 - empty
        1 - water
        2 - road
        3 - railway




        .. note:: gis-wise operations need to be treated carefully, as the object representations differ:

        - Waters are likely to be stored in multipolys

          For Waters it's important to check which:
             - Crumbs INTERSECT with the water-area
             - Crumbs are COVEREDBY the water-area

        - Roads and Railways are stored as multilinestrings
          For (roads|railways) it's important to check which:
             - Crumbs INTERSECT with the (roads|railways)-linestring
             - Crumbs CROSS the (roads|railways)-linestring
             #- Crumbs "TOUCH" the (roads|railways)-linestring
            => just a thought: is there a spatial lookup functions that checks whether the geometries barely touch? or is this handled by INTERSECT?

        :return: 2-dimensional array of the below map layer in json/dict format
        :rtype: dict

        """
    
        self._below = self.gen_lvl()

        self._water()
        self._road()
        self._railway()

        return json.dumps(self._below.tolist())

    def equals(self):
    
        """

        return the part that's on equal height as the player (the layer he collides with)

        lookup code draft:
        0 - empty
        1 - building

        :return: 2-dimensional array of the equals map layer in json/dict format
        :rtype: dict

        """
    
        self._equals = self.gen_lvl()

        self._building()

        return json.dumps(self._equals.tolist())

    def to_level(self):

        """

        Do all neccessary calculations and save the result as a game.Level instance.

        """

        mapdata = {}

        mapdata["below"] = json.loads(self.below())
        mapdata["equals"] = json.loads(self.equals())
        mapdata["above"] = []

        lvl = Level()
        lvl.name = self.name + " Level"
        lvl.width = self.width
        lvl.height = self.height
        lvl.data = mapdata

        lvl.save()
