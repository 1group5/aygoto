"""
lvl -- Tool for automated Level creation
"""

import djclick as click

import sys

from gameworld.models import *


@click.command()
@click.option("--lat", default=None, help="Latiude of the centroid of your first Chunk")
@click.option(
    "--lon", default=None, help="Longitude of the centroid of your first Chunk"
)
@click.argument("name")
def command(name, lat, lon):
    """
    lvl.command

    construct the Bunch including Level output data by gi

    :param name: str, the name of the Chunk/ LevelMapping/ Level to create
    :param lat: float, the latitude of the Chunk's center
    :param lon: float, the longitude of the Chunk's center
    """
    if lat is None or lon is None:
        click.secho("[!] need to specify --lat and --lon !", fg="red")
        sys.exit(1)

    # click.secho("Hello", fg="red")
    click.secho("[DBG]: \nlat: {}\nlon: {}\n".format(lat, lon), fg="red")

    # TODO: account for Chunk being already present:
    # something like:
    # created, c = Chunk.objects.get_or_create()
    click.secho("[*] Constructing Chunk", fg="blue")
    # p = (lat, lon)  # TODO: triple check this so we're not on somalias coastline again!
    p = (
        float(lon),
        float(lat),
    )  # TODO: triple check this so we're not on somalias coastline again!
    c = Chunk()
    c.name = str(name)  # TODO: fix type conversion
    c.set_centroid(p)
    c.construct()
    c.save()

    # gen_crumbs
    if not c.has_crumbs():
        with click.progressbar(range(c.size * c.size)) as bar:
            c.gen_crumbs()

            num = Crumb.objects.filter(chunk=c).count()

            bar.update(num)

    click.secho("[*] Generating LevelMapping...", fg="blue")
    # create LevelMapping
    lm = LevelMapping()
    lm.name = c.name + " LevelMapping"
    lm.chunk = c
    lm.save()

    click.secho("[*] Generating Level...", fg="blue")
    # Create Level
    lm.to_level()
    click.secho(30 * "#", fg="blue")
    click.secho("[=] All Done.", fg="blue")
    click.secho(30 * "#", fg="blue")
