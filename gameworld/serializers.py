from rest_framework import serializers

from .models import Chunk
from .models import Crumb



class ChunkSerializer(serializers.ModelSerializer):
    '''
    Serialize WorldBorder Objects
    '''

    class Meta:
        model = Chunk
        fields = '__all__'

class CrumbSerializer(serializers.ModelSerializer):
    '''
    Serialize WorldBorder Objects
    '''

    class Meta:
        model = Crumb
        fields = '__all__'