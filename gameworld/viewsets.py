from rest_framework import viewsets

from .models import Chunk
from .models import Crumb
from .serializers import ChunkSerializer
from .serializers import CrumbSerializer


class ChunkViewset(viewsets.ModelViewSet):
    """
    Acces on Chunk api endpoint
    """

    queryset = Chunk.objects.all()
    serializer_class = ChunkSerializer


class CrumbViewsets(viewsets.ModelViewSet):
    """
    Acces on Crumb api endpoint
    """

    queryset = Crumb.objects.all()
    serializer_class = CrumbSerializer
