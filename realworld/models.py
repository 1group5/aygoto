from django.contrib.gis.db import models
from osm_field.fields import LatitudeField, LongitudeField, OSMField

# for test use just roads water and waterways
# roads

# REVIEW: lets talk about whether we want to name the geo-fields accordingly, like:
# + mpoint -- for MultiPoint Types
# + mline -- for MultiLine Types
# + mpoly -- for MultiPolygon Types
#
# OR whether we want it to be called 'geom' on every model


class Road(models.Model):
    """
    The Model for facialiating Roads. This class also holds metadata about the Road
    like whether it's an oneway-road or the maximum speed allowed on that road.

    :type geom: A MultiLineString that connects the Points a Road consists of
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    ref = models.CharField(max_length=20, null=True)
    oneway = models.CharField(max_length=1)
    maxspeed = models.IntegerField()
    layer = models.BigIntegerField()
    bridge = models.CharField(max_length=1)
    tunnel = models.CharField(max_length=1)
    geom = models.MultiLineStringField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_roads model
road_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "ref": "ref",
    "oneway": "oneway",
    "maxspeed": "maxspeed",
    "layer": "layer",
    "bridge": "bridge",
    "tunnel": "tunnel",
    "geom": "MULTILINESTRING",
}

# water
class Water(models.Model):
    """
    Model representation of OSM's Water key

    see https://wiki.openstreetmap.org/wiki/Key:water
    for more information.

    This unifies many kinds of waters, like:

    + lake
    + river
    + oxbow
    + lagoon
    + stream
    + stream_pool
    + basin
    + canal
    + pond
    + ...

    :type geom: The area where water is present
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_water model
water_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "geom": "MULTIPOLYGON",
}

# waterways
class Waterway(models.Model):
    """
    Like streets, but on water.

    :type geom: MutliLineString that connects the points that make up a WaterWay
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    width = models.IntegerField()
    name = models.CharField(max_length=100, null=True)
    geom = models.MultiLineStringField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_waterways model
waterway_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "width": "width",
    "name": "name",
    "geom": "MULTILINESTRING",
}

# landuse a
class Landuse(models.Model):
    """
    Model representing OSM's Landuse Key.

    see https://wiki.openstreetmap.org/wiki/Key:landuse
    for more information.

    This specifies as how a certain area is used.
    Like:

    + industrial
    + residential
    + cementery
    + military
    + ...

    :type geom: The area of designated usage

    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_landuse model
landuse_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "geom": "MULTIPOLYGON",
}

# natural a


class Terrain(models.Model):
    """
    Model mapping to OSM's natural key.

    see https://wiki.openstreetmap.org/wiki/Key:natural
    for more information.

    This holds information of what natural terrain a certain area is of, like:

    + Wood
    + Tundra
    + ...

    NOTE: this model is renamed to our purposes.
    Source shp-files and OSM will refer to this type of Model as 'Natural'

    :type geom: The Area that defines a certain Terrain
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_natural_A model
terrain_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "geom": "MULTIPOLYGON",
}


# places
class Place(models.Model):
    """
    Model Representation of a place.

    :type geom: The area that belongs to a certain place
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    population = models.BigIntegerField()
    name = models.CharField(max_length=100, null=True)
    geom = models.MultiPointField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_places model
place_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "population": "population",
    "name": "name",
    "geom": "MULTIPOINT",
}


# pois
class PointOfInterest(models.Model):
    """
    Model holding Points of Interest

    see https://wiki.openstreetmap.org/wiki/Points_of_interest
    for more information.

    :type geom: The 1 (or more) Point(s) that is of interest
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    # FIXME: datasources vary in MultiPoint/ Polygon field...
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Point of Interest"
        verbose_name_plural = "Points of Interest"


# Interest-generated `LayerMapping` dictionary for Gis_osm_pois model
pointofinterest_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "geom": "MULTIPOLYGON",
}

# railways
class Railway(models.Model):
    """
    Like roads but for trains!

    :type geom: Multi Line String that connects the Points that make up a Railway

    see https://wiki.openstreetmap.org/wiki/Key:railway
    for additional keys and types what kind of railways are inside OSM.
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    layer = models.BigIntegerField()
    bridge = models.CharField(max_length=1)
    tunnel = models.CharField(max_length=1)
    geom = models.MultiLineStringField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_railways model
railway_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "layer": "layer",
    "bridge": "bridge",
    "tunnel": "tunnel",
    "geom": "MULTILINESTRING",
}

# traffic
class Traffic(models.Model):
    """
    The amount of vehicles that transit a road

    as https://wiki.openstreetmap.org/wiki/Key:traffic
    states it:

    + traffic=trunk >= 10000
    + traffic=heavy < 10000
    + traffic=intermediate < 5000
    + traffic=low < 2000

    vehicles per day.

    :type geom: 1 or multiple points on which this traffic is measured/ vehicles are passing
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    # FIXME: datasources vary in MultiPoint/ Polygon field...
    geom = models.MultiPointField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_traffic model
traffic_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "geom": "MULTIPOINT",
}


# transport
class PublicTransport(models.Model):
    """
    Model holding information about Public Transport, like Busses, Trams, Trains, ...

    see https://wiki.openstreetmap.org/wiki/Public_transport
    for more information.

    :type geom: 1+ points that holds information about Public Transportation
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    # FIXME: datasources vary in MultiPoint/ Polygon field...
    geom = models.MultiPointField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_transport model
publictransport_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "geom": "MULTIPOINT",
}


# integrity error
# buildings a
class Building(models.Model):
    """
    Model representing where buildings are located.

    see https://wiki.openstreetmap.org/wiki/Key:building
    for more information.

    there are various kinds of buildings, like:

    + appartment
    + bungalow
    + office
    + cathedral
    + ...

    :type geom: MultiPoly that holds the area of a certain building
    """

    osm_id = models.CharField(max_length=10)
    code = models.IntegerField()
    fclass = models.CharField(max_length=28)
    name = models.CharField(max_length=100, null=True)
    type = models.CharField(max_length=20, null=True)
    geom = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return str(self.name)


# Auto-generated `LayerMapping` dictionary for Gis_osm_buildings_A model
building_mapping = {
    "osm_id": "osm_id",
    "code": "code",
    "fclass": "fclass",
    "name": "name",
    "type": "type",
    "geom": "MULTIPOLYGON",
}


# REVIEW: might be done somewhat?
# TODO: specify further
# OSMGEOXX
#
# WorldBorder
# City
# Road
# River
#
#
# I want map for X:WorldBorder
#
# X? -> X:X (Land)     [0,1,0,1,0,0]
# City? -> X:X (Cities..)        [0,1,1,0]
# Road? -> X:X (Roads..)
# City? -> X:X (Cities..)
# This is an auto-generated Django model module created by ogrinspect.


class WorldBorder(models.Model):
    """
    The WorldBorder Model from the official GeoDjango Tutorial

    see https://docs.djangoproject.com/en/3.2/ref/contrib/gis/tutorial/#

    for more information
    """

    # just the usual pure-django model fields:
    fips = models.CharField(max_length=2, null=True, default=None)
    iso2 = models.CharField(max_length=2)
    iso3 = models.CharField(max_length=3)
    un = models.IntegerField()
    name = models.CharField(max_length=50)
    area = models.IntegerField()
    pop2005 = models.BigIntegerField()
    region = models.IntegerField()
    subregion = models.IntegerField()
    lon = models.FloatField()
    lat = models.FloatField()
    # GeoDjango-specific field
    mpoly = models.MultiPolygonField(srid=4326)

    def __str__(self):
        return self.name


# Auto-generated `LayerMapping` dictionary for WorldBorder model
worldborder_mapping = {
    "fips": "FIPS",
    "iso2": "ISO2",
    "iso3": "ISO3",
    "un": "UN",
    "name": "NAME",
    "area": "AREA",
    "pop2005": "POP2005",
    "region": "REGION",
    "subregion": "SUBREGION",
    "lon": "LON",
    "lat": "LAT",
    "mpoly": "MULTIPOLYGON",
}

GEOMODEL_CHOICES = [
    Road,
    Water,
    Terrain,
    Landuse,
]
