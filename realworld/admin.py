from django.contrib.gis import admin
from .models import WorldBorder
from .models import Building, \
                    Road, \
                    Water, \
                    Waterway, \
                    Railway, \
                    Landuse, \
                    Terrain, \
                    Place, \
                    PointOfInterest \

# Register your models here.

myModels = [
    WorldBorder,
    Road,
    Water,
    Waterway,
    Railway,
    Landuse,
    Terrain,
    Place,
    PointOfInterest,
]

admin.site.register(myModels, admin.GeoModelAdmin)
