import datetime
import os
from pathlib import Path

from django.contrib.gis.utils import LayerMapping
from .models import WorldBorder, worldborder_mapping


def load(model, shp_file, mapping, transform=False, verbose=True):
    """
    a generic load function that loads any type of geo-data, given a Model, a mapping and a shp-file

    :param model: the Model inside which data should be stored
    :param shp_file: the file where the data originates from
    :param mapping: the mapping to use. handles conversion between shp-file names and Model fields
    :param transform: whether or not to transform the given data into another Spatial Ref ID
    :param verbose: whether to save verbosely (echo saved models)
    """
    print("[DBG] loading {}".format(shp_file))
    lay_map = LayerMapping(model, shp_file, mapping, transform=transform)
    lay_map.save(strict=True, verbose=verbose)


def rp(fname):
    """
    return the resolved path (OS-independently)

    :rtype rp: str, os-specific resolved Path()
    """
    return str(Path(fname).resolve())


def ap(fname):
    """
    return the absolute path

    :rtype ap: str, os-specific absolute Path
    """
    return str(os.path.abspath(fname))


def fname(basename):
    """
    resolve the path of a given base filename

    :param basename: the base filename that should be searched for

    :rtype shp_realpath: the absolute path of the file, if present
    """
    shp_realpath = os.path.join(Path(__file__).resolve().parent, "data", basename)
    return shp_realpath


def world_borders(verbose=True):
    """
    import world borders

    :param verbose: whether the LayerMapping should be constructed verbosely, defaults to True
    :type verbose: bool, optional
    """
    load(WorldBorder, fname("TM_WORLD_BORDERS-0.3.shp"), worldborder_mapping)
