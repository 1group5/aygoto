from rest_framework import serializers

from .models import WorldBorder


class WorldBorderSerializer(serializers.ModelSerializer):
    '''
    Serialize WorldBorder Objects
    '''

    class Meta:
        model = WorldBorder
        fields = '__all__'
