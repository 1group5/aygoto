from django.core.management.base import BaseCommand, CommandError
from django.utils.text import slugify


from realworld import load
from realworld.models import WorldBorder


class Command(BaseCommand):
    '''
    Custom management command for initializing the world.

    It calls the load-mechanism of static data sources to populate the DB.

    TODO: add arg-handling
          loading mechanisms for other objects


    '''
    help = (
        'Initialize the world'
    )

    def handle(self, *args, **options):
        # we don't want duplicate countries
        if WorldBorder.objects.count() == 0:
            load.world_borders()
            self.stdout.write(self.style.SUCCESS('[+] Worldborders initialized.'))
        else:
            self.stdout.write(self.style.WARNING('[*] Worldborders already present.'))
