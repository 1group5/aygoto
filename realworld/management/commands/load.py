"""
load.py -- management command for loading geodata from local files



python manage.py load <FIILE.shp>
"""

import sys
from pathlib import Path

import djclick as click

# from realworld.models import GEOMODEL_CHOICES
from realworld.models import *
from realworld.load import load, ap

geofabrik_mapping = {
    "gis_osm_buildings_a_free_1": ("Building", "building_mapping"),
    "gis_osm_landuse_a_free_1": ("Landuse", "landuse_mapping"),
    "gis_osm_natural_a_free_1": ("Terrain", "terrain_mapping"),
    "gis_osm_natural_free_1": ("Terrain", "terrain_mapping"),
    "gis_osm_places_a_free_1": ("Place", "place_mapping"),
    "gis_osm_places_free_1": ("Place", "place_mapping"),
    "gis_osm_pois_a_free_1": ("PointOfInterest", "pointofinterest_mapping"),
    "gis_osm_pois_free_1": ("PointOfInterest", "pointofinterest_mapping"),
    "gis_osm_railways_free_1": ("Railway", "railway_mapping"),
    "gis_osm_roads_free_1": ("Road", "road_mapping"),
    "gis_osm_traffic_a_free_1": ("Traffic", "traffic_mapping"),
    "gis_osm_traffic_free_1": ("Traffic", "traffic_mapping"),
    "gis_osm_transport_a_free_1": ("PublicTransport", "publictransport_mapping"),
    "gis_osm_transport_free_1": ("PublicTransport", "publictransport_mapping"),
    "gis_osm_water_a_free_1": ("Water", "water_mapping"),
    "gis_osm_waterways_free_1": ("Waterway", "waterway_mapping"),
}


@click.command()
# @click.argument('f', help="The file to ingest")
@click.argument("f")
def command(f):
    # TODO: handle geom type inference
    click.secho("Hello", fg="red")
    click.secho("Hello", fg="blue")
    # p = Path(f).resolve()
    p = ap(f)
    print("[ÐBG] p: {}".format(p))
    click.echo(f)
    click.secho("{}".format(p), fg="yellow")

    if f.endswith(".shp"):
        fname = Path(f).resolve().parts[-1]
        click.secho("[DBG] fname: {}".format(fname), fg="yellow")
        base, ext = fname.split(".")
        click.secho("[DBG]: base: {}\next: {}".format(base, ext), fg="red")

        if base in geofabrik_mapping.keys():
            model, mapping = geofabrik_mapping[base]
            click.secho(
                "{}:> got {}; Suggesting: {} with {}".format(f, base, model, mapping),
                fg="yellow",
            )

            load(eval(model), p, eval(mapping))
