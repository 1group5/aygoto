#!/bin/bash
link=https://thematicmapping.org/downloads/TM_WORLD_BORDERS-0.3.zip
file=TM_WORLD_BORDERS-0.3.zip
deps=unzip
shapes=TM_WORLD_BORDERS-0.3.shp
printf  "Checking if zip exists...\n"
if test -f $file; then
    printf "main file exists\n"
else 
    printf  "Downloading...\n"
    wget $link
fi
printf  "Checking files...\n"
if test -f $shapes; then
    printf "shapes exists\n"
    exit 0
else
    printf  "Checking dependencies... \n"
    if  [[ $(which $deps 2>/dev/null) ]] ;then 
        printf  "Dependencies confirmed.... unzipping\n"
        unzip $file
    else
        printf  "please install $dpes\n"
        exit 1
    fi
fi
exit 0
