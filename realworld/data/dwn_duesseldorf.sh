#!/bin/bash
#simple script to automate shp downloads
link=http://download.geofabrik.de/europe/germany/nordrhein-westfalen/duesseldorf-regbez-latest-free.shp.zip
file=*latest-free.shp.zip
deps=unzip
shapes=gis_osm_buildings_a_free_1.shp
printf  "Checking if zip exists...\n"
if test -f $file; then
    printf "main file exists\n"
else 
    printf  "Downloading...\n"
    wget $link
fi
printf  "Checking files...\n"
if test -f $shapes; then
    printf "shapes exists\n"
    exit 0
else
    printf  "Checking dependencies... \n"
    if  [[ $(which $deps 2>/dev/null) ]] ;then 
        printf  "Dependencies confirmed.... unzipping\n"
        unzip $file
    else
        printf  "please install $dpes\n"
        exit 1
    fi
fi
exit 0
