'''
Viewsets for realword -- controlling access on everything the realworld api serves
'''

from rest_framework import viewsets

from .models import WorldBorder
from .serializers import WorldBorderSerializer

class WorldBorderViewset(viewsets.ModelViewSet):
    '''
    Manage access on WorldBorder API Endpoint
    '''
    queryset = WorldBorder.objects.all()
    serializer_class = WorldBorderSerializer
