from django.urls import path, include

from rest_framework import routers

from realworld.viewsets import WorldBorderViewset
from gameworld.viewsets import ChunkViewset, CrumbViewsets
from game.viewsets import LevelViewset

router = routers.DefaultRouter()
router.register("worldborder", WorldBorderViewset)
router.register("chunk", ChunkViewset)
router.register("crumb", CrumbViewsets)
router.register("level", LevelViewset)


urlpatterns = [
    path("", include(router.urls)),
    path("api-auth", include("rest_framework.urls", namespace="rest_framework")),
]
