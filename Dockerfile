############################################################
### NOTE: the Dockerfile is now composed of different stages
### that eliminate the need for manual intervention in tasks
### like test execution, doc creation, etc...
############################################################


#############################################################################
### BUILDER  -- the base target, pure in its form, what we all know and love
#############################################################################
FROM python:3.8.5 AS builder

RUN apt update && apt -y install \
                            # for postgres
                            libpq-dev \
                            python3-dev \
                            # for postgis
                            libgeos-dev \
                            libproj-dev \
                            libgdal-dev


RUN pip install pipenv

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY Pipfile* ./
#RUN pipenv shell
RUN pipenv install --system --deploy --ignore-pipfile


RUN mkdir -p /usr/src/app/aygoto
COPY . /usr/src/app/aygoto
WORKDIR /usr/src/app/aygoto

RUN python manage.py collectstatic --noinput

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]


############################################################
###  DEV -- builder + all the dev deps
############################################################
FROM builder as dev

RUN apt update && apt -y install \
                            # for jsdocs
                            nodejs \
                            npm

RUN npm install jsdoc -g

RUN pipenv install --dev --system --deploy --ignore-pipfile

############################################################
###  TEST -- just run them
############################################################
FROM dev as test

RUN pip3 install pytest

#RUN apt update && apt -y install \
                            # for jsdocs
                            #nodejs \
                            #npm \

CMD ["/usr/local/bin/pytest"]


############################################################
###  DOC -- build the documentation
############################################################
FROM dev as doc

COPY ./docs/ /usr/src/app/aygoto/docs


WORKDIR /usr/src/app/aygoto/docs

CMD ["make", "html"]
