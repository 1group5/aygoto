Aygoto
======

a game.

NEWS:
-----

-  ``proto`` app under construction
-  push notifications via tg :)

Getting started
---------------

To spin up local dev version

::


   git clone https://gitlab.com/1group5/aygoto

   cd aygoto

   pipenv shell
   pipenv install
   pipenv install --dev

   python manage.py createsuperuser
   python manage.py makemigrations
   python manage.py migrate

   python manage.py collectstatic

   python manage.py runserver

Starting off in containers (docker + docker-compose) is even simpler:

.. code:: sh

   make build up migrate restart

Architecture
------------

::

   aygoto
     -> proto 
     -> map 

Build the docs
--------------

::


   git clone https://gitlab.com/1group5/aygoto

   cd aygoto/docs ; pipenv install --dev

   # NOTE that you also need 'jsdoc' binary installed for fetching .js docsstrings
   sudo npm i -g jsdoc
   sudo pacman -S texlive-most
   make 

   make html
   make latexpdf

Run the tests (WORK IN PRORESS!)
--------------------------------

::


   git clone https://gitlab.com/1group5/aygoto

   cd aygoto; pipenv install --dev


   pytest


Temporaray way of loading .shp (shapefiles) into Database
---------------------------------------------------------

::

   python(3) manage.py load absolute/path/to/shp


Create Chunk and check whats inside
-----------------------------------

::

   pipenv shell 
   pipenv install
   pipenv install --dev

   python3 manage.py shell_plus --ipython

    p = (57.XXXX,6.9XXX)
    c = Chunk()
    c.name = " Razam Test Chunk"
    c.set_centroid(p)
    c.construct()
    c.save()
    c.gen_crumbs()
    c.has_crumbs()
    c.save()

    l = LevelMapping()
    l.name = "Razam lvl map test"
    l.chunk = c
    l.save()
    l.to_level()

   # via REPL
    lvl = Level.objects.first()
    lvl.data

   # via curl
   $ curl localhost:8000/api/level/

Removing Chunk(s)
-----------------

::

   Chunk(s) created [x]
   c.delete()

   or

   for c in Chunk.objects.all():
   c.delete()
